# Changelog

## [0.0.4] - 2023-07-31

### Agregado

- Modelo Account
- Modelo AccountDetail
- Repositorio para el manejo del ws account [AccountRepository] método Get, Put

## [0.0.3] - 2023-05-31

- Modificada la clase Parameters agregando campos de consulta para personas

## [0.0.2] - 2023-04-24

### Agregado

- Agregado al método toJson el id del Usuario
- Agregado la dirección a la Visita
- Agregado a la clase VisitResult el id de la Cuenta

### Modificado

- Modificado el toJson de Visita para agregar la dirección al momento de actualizar
- Modificado el toJson del Resultado de la visita agregando el id de la cuenta

## Eliminado

- Eliminada la función de permite finalizar visita

## [0.0.1] - 2023-04-08

### Agregado

- Correccion ApiProvider visualización de errores
- Nuevos modelos para consumir webservice de auth