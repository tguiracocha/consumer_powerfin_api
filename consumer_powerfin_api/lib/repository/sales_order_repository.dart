import 'package:consumer_powerfin_api/client/api_provider.dart';
import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';
import 'package:consumer_powerfin_api/model/item.dart';
import 'package:consumer_powerfin_api/model/person/external_user.dart';
import 'package:consumer_powerfin_api/model/sale_order.dart';
import 'package:dio/dio.dart';

class SaleOrderRepository {
  final Dio dio;

  SaleOrderRepository(this.dio);

  //Listar ordenes de venta por: Id, User, Status

  Future<List<SaleOrder>> getSaleOrdersList(
      Parameters parameters, ExternalUser externalUser) async {
    var json = await ApiProvider.getData(
        dio: this.dio,
        filter: '/orders/sales',
        parameters: parameters.toJson());

    return SaleOrder.getSaleOrders(json, externalUser);
  }

  //Cambiar el Status de una Orden de Venta: Autorizar - Cancelar
  //Autorizar => Generar una Factura en estado Solicitado

  Future<String> postCancelSaleOrder(String saleOrderId) async {
    var json = await ApiProvider.postData(
        dio: this.dio, filter: '/orders/$saleOrderId/cancel-order', data: {});

    return json[0]['id'];
  }

  Future<Map<String, String>> postAuthorizeSaleOrder(
      SaleOrder saleOrder) async {
    var json = await ApiProvider.postData(
        dio: this.dio,
        filter: '/orders/sales/${saleOrder.id}/authorize-order',
        data: saleOrder.toJsonSaleOrder());

    return {
      "accountId": "${json[0]['accountInvoiceid']}",
      "code": "${json[0]['code']}",
      "reportId": "${json[0]['reportId']}",
    };
    //return json[0]['id'];
  }

  //Crear una Orden de Venta en estado SOLICITADO

  Future<String> postCreateNewSaleOrder(SaleOrder saleOrder) async {
    var json = await ApiProvider.postData(
        dio: this.dio,
        filter: '/orders/sales',
        data: saleOrder.toJsonSaleOrder());

    return json[0]['code'];
  }

  //WEB SERVICE PARA EL DETALLE DE LA ORDEN DE VENTA
  //Listar items de la orden de venta

  Future<List<Item>> getItemsSaleOrder(String idSaleOrder) async {
    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/orders/$idSaleOrder/details', parameters: {});

    return Item.getItemList(json, isDetail: true);
  }

  //Agregar un nuevo item a la Orden de Venta

  Future<dynamic> posAddNewItemSaleOrder(String idSaleOrder, Item item) async {
    var json = await ApiProvider.postData(
        dio: this.dio,
        filter: '/orders/$idSaleOrder/details',
        data: item.toJson());

    return json[0]['id'];
  }

  //Actualizar el item de la orden de venta

  Future<void> putItemsSaleOrder(String idSaleOrder, Item item) async {
    await ApiProvider.putData(
        dio: this.dio,
        filter: '/orders/$idSaleOrder/details',
        data: item.toJson());

    return;
  }

  // Eliminar un item de la Orden de Venta

  Future<void> deleteItemsSaleOrder(String idSaleOrder, Item item) async {
    await ApiProvider.deleteData(
        dio: this.dio,
        filter: '/orders/$idSaleOrder/details/${item.detailId}',
        data: {});

    return;
  }
}
