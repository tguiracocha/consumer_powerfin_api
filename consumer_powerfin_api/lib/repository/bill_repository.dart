
import 'package:consumer_powerfin_api/client/api_provider.dart';
import 'package:consumer_powerfin_api/model/invoice.dart';
import 'package:consumer_powerfin_api/model/sale_collected.dart';
import 'package:dio/dio.dart';

class BillRepository {

  final Dio dio;

  BillRepository(this.dio);


  //Facturas Vencidas por Vendedor

  Future<List<Invoice>> getOverdueSaleByUser(
      {String user}) async {
    try {
      var jsonItems = await ApiProvider.getData(
          filter: '/users/$user/overdue-sales', parameters: {}, dio: this.dio);

      List<Invoice> overdueBillList = Invoice.getInvoiceList(jsonItems);

      return overdueBillList;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  //Facturas Vencidas por persona

  Future<List<Invoice>> getOverdueSaleByPerson(
      {String personId, String token}) async {
    try {
      var jsonItems = await ApiProvider.getData(
          filter: '/persons/$personId/overdue-sales',
          parameters: {},
          dio: this.dio);

      List<Invoice> overdueBillList = Invoice.getInvoiceList(jsonItems);

      return overdueBillList;
    } catch (e) {
      throw Exception(e.toString());
    }
  }


  Future<List<SaleCollected>> getSalesCollected(
      {String user}) async {
    try {
      var jsonItems = await ApiProvider.getData(
          filter: '/users/$user/sales-collected', parameters: {}, dio: dio);

      List<SaleCollected> saleCollectedList =
          SaleCollected.getSaleCollectedList(jsonItems);

      return saleCollectedList;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<void> payInvoice(
      {Invoice invoice, double balance}) async {
    try {
      await ApiProvider.postData(
          filter: '/invoices/sales/${invoice.id}/payment',
          data: {
            "paymentValue": '$balance',
            "collectionBox": invoice.collectionBox
          },
          dio: dio);
      return;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}
