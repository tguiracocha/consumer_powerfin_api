import 'package:consumer_powerfin_api/client/api_provider.dart';
import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';
import 'package:consumer_powerfin_api/model/customer_type.dart';
import 'package:consumer_powerfin_api/model/person/customer.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class PersonRepository {
  final Dio dio;

  PersonRepository(this.dio);

  Future<void> registerCustomer({
    Customer customer,
  }) async {
    try {
      var data = customer.toJson();
      await ApiProvider.postData(
        filter: '/persons',
        dio: this.dio,
        data: data,
      );
    } catch (e) {
      throw e;
    }
  }

  Future<void> updateCustomer(
      {@required String token,
      Customer customer,
      CustomerType customerType}) async {
    try {
      var data = customerType.toJsonCustomerType(customer);

      await ApiProvider.putData(
        filter: '/persons',
        dio: this.dio,
        data: data,
      );
    } catch (e) {
      throw e;
    }
  }

  Future<List<Customer>> searchCustomer(
      {@required Parameters parameters}) async {
    try {
      var jsonPerson = await ApiProvider.getData(
        filter: '/persons',
        dio: this.dio,
        parameters: parameters.toJson(),
      );

      return Customer.getCustomerList(jsonPerson);
    } catch (e) {
      return e;
    }
  }
}
