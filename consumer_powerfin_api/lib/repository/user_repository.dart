
import 'package:consumer_powerfin_api/client/api_provider.dart';
import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';
import 'package:consumer_powerfin_api/model/person/external_user.dart';
import 'package:consumer_powerfin_api/model/user_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class UserRepository {
  final Dio dio;

  UserRepository(this.dio);

  Future<String> authenticate({@required User user}) async {
    try {
      return await ApiProvider.postData(
          filter: '/users/login', isLogin: true, data: user.toJson(), dio: this.dio);
    } catch (e) {
      throw e;
    }
  }

  Future<String> recoverIdUser() async {
    try {
      var json = await ApiProvider.getData(
          filter: '/users/rebuild', dio: this.dio, parameters: {});
      return json[0]['userName'].toString();
    } catch (e) {
      throw e;
    }
  }

  Future<ExternalUser> getPersonalInformation(
      {@required String idUser}) async {
   try{
     var jsonPerson = await ApiProvider.getData(
         filter: '/users/$idUser/', dio: this.dio, parameters: {});

     return ExternalUser.fromJson(jsonPerson[0]);
   }catch (e){
     throw e;
   }
  }

  Future<String> getAssignedCity(
      {@required String idUser}) async {
    try{
      var jsonPerson = await ApiProvider.getData(
          filter: '/users/$idUser/', dio: this.dio, parameters: {});

      return jsonPerson[0]['assignedCityId'].toString();
    }catch (e){
      throw e;
    }
  }

  Future<void> changePasswordUser(
      { @required String idUser,
        @required Map<String, String> data}) async {
    try {
      return await ApiProvider.putData(
          filter:'/users/$idUser/change-password', dio: this.dio, data: data);
    } catch (e) {
      throw e;
    }
  }

  Future<void> sendUser({@required String idUser}) async {
    try {
      await ApiProvider.getData(
          filter: '/users/$idUser/reset-password-code/', dio: this.dio, parameters: {});
    } catch (e) {
      throw e;
    }
  }

  Future<void> resetPassword(
      {@required String idUser, @required Map<String, String> data}) async {
    try {
      await ApiProvider.postData(
          filter: '/users/$idUser/reset-password/', dio: this.dio, data: data);
    } catch (error) {
      throw error;
    }
  }

  Future<String> disableUser(String userName, Map<String, String> data) async {

    var json = await ApiProvider.putData(
        dio: this.dio,
        filter: '$userName/disable-user',
        data: data);

    return json[0]['message'];
  }

  Future<String> enableUser(Map<String, String> data) async {

    var json = await ApiProvider.putData(
        dio: this.dio,
        filter: 'enable-user',
        data: data);

    return json[0]['message'];
  }

}
