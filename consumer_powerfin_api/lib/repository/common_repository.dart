import 'package:consumer_powerfin_api/model/commons.dart';
import 'package:consumer_powerfin_api/model/person_type.dart';
import 'package:dio/dio.dart';
import 'package:consumer_powerfin_api/client/api_provider.dart';
import 'package:consumer_powerfin_api/model/financial_entity.dart';
import 'package:consumer_powerfin_api/model/commons/account_bank_type.dart';
import 'package:consumer_powerfin_api/model/customer_type.dart';
import 'package:consumer_powerfin_api/model/gender.dart';
import 'package:consumer_powerfin_api/model/home_types.dart';
import 'package:consumer_powerfin_api/model/marital_status.dart';
import 'package:consumer_powerfin_api/model/identification_type.dart';
import 'package:consumer_powerfin_api/model/district.dart';
import 'package:consumer_powerfin_api/model/duedate_invoice.dart';
import 'package:consumer_powerfin_api/model/zone.dart';
import 'package:consumer_powerfin_api/model/payment_method.dart';
import 'package:consumer_powerfin_api/model/visit_result.dart';
import 'package:consumer_powerfin_api/model/visit_type.dart';
import 'package:consumer_powerfin_api/model/payment_type_invoice.dart';
import 'package:consumer_powerfin_api/model/isp.dart';
import 'package:consumer_powerfin_api/model/zone_isp.dart';
import 'package:consumer_powerfin_api/model/phone_number_type.dart';

///=====================================================================
///Repositorio para consumir los WebServices de commons para
///los formularios de registro persona, contrato de servicio de internet,
///direccion de una persona
///=====================================================================

class CommonRepository {

  final Dio dio;

  CommonRepository(this.dio);

  Future<List<FinancialEntity>> getFinancialEntities(String term) async {
    var jsonProduct = await ApiProvider.getData(
        dio: this.dio,
        filter: '/common/financial-entities',
        parameters: {
          'name': '$term',
        });

    return FinancialEntity.getFinantialEntityList(jsonProduct);

  }

  Future<List<AccountBankType>> getAccountBankType() async {

    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/common/account-bank-types', parameters: {});

    return AccountBankType.getAccountBankTypeList(json);

  }

  Future<List<CustomerType>> getCustomerType() async {
    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/common/customer-types', parameters: {});

    return CustomerType.getCustomerTypeList(json);
  }

  Future<List<Gender>> getGenders() async {
    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/common/genders', parameters: {});

    return Gender.getGenderList(json);
  }

  Future<List<HomeType>> getHomeTypes() async {
    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/common/home-types', parameters: {});

    return HomeType.getHomeTypesfromJson(json);
  }

  Future<List<MaritalStatus>> getMaritalStatus() async {
    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/common/marital-status', parameters: {});

    return MaritalStatus.getMaritalStatusList(json);
  }

  Future<List<IdentificationType>> getIdentificationTypes() async {
    var jsonProduct = await ApiProvider.getData(
        dio: this.dio, filter: '/common/identification-types', parameters: {});

    return IdentificationType.getIdTypeList(jsonProduct);
  }

  Future<Zone> getZone(String assignedCityId) async {
    var json = await ApiProvider.getData(
        dio: this.dio,
        filter: '/common/cities/$assignedCityId',
        parameters: {});

    return Zone.fromJson(json[0]);
  }

  Future<List<District>> getDistricts(Zone zone) async {
    var json = await ApiProvider.getData(
        dio: this.dio,
        filter: '/common/districts',
        parameters: Zone.toJson(zone));

    return District.getDistrictsfromJson(json);
  }

  Future<List<District>> searchDistricts(String parameter) async {
    var json = await ApiProvider.getData(
        dio: this.dio,
        filter: '/common/districts',
        parameters: {'name': '$parameter'});

    return District.getDistrictsfromJson(json);
  }

  Future<List<DueDateInvoice>> getDueDateByAmount(
      double amount, String issueDate, int personId,
      {String customerTypeId}) async {
    final Map<String, String> parameters = new Map<String, String>();

    parameters["amount"] = "$amount";
    parameters["issueDate"] = issueDate;

    if (customerTypeId != null) parameters["CustomerTypeId"] = customerTypeId;
    if (personId != null) parameters["personId"] = "$personId";


    var json = await ApiProvider.getData(
        dio: this.dio,
        filter: '/common/due-dates-by-amount',
        parameters: parameters);

    return DueDateInvoice().getDueDates(json);
  }

  Future<List<PaymentTypeInvoice>> getPaymentTypesInvoice() async {
    var json = await ApiProvider.getData(
        dio: this.dio,
        filter: '/common/payment-types-for-invoice',
        parameters: {});

    return PaymentTypeInvoice.getPaymentTypeList(json);
  }


  Future<List<PaymentMethod>> getPaymentMethodExternalSale() async {
    var json = await ApiProvider.getData(
        dio: this.dio,
        filter: '/common/payment-methods-external-sales',
        parameters: {});

    return PaymentMethod.getPaymentMethodList(json);
  }

  Future<List<VisitType>> getVisitTypeList() async {
    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/common/visit-types', parameters: {});

    return VisitType.getVisitTypeList(json);
  }

  Future<List<VisitResult>> getVisitResultList() async {
    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/common/visit-result-types', parameters: {});

    return VisitResult.getVisitResult(json);
  }

  Future<List<InternetServicePlan>> getispList() async {
    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/common/isp-services', parameters: {});

    return InternetServicePlan.getISPList(json);
  }

  Future<List<ZoneISP>> getZoneISPList() async {
    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/common/zones', parameters: {});

    return ZoneISP.getZoneISPfromJson(json);
  }

  Future<List<PhoneNumberType>> getPhoneNumberTypeList() async {
    var json = await ApiProvider.getData(
        dio: this.dio, filter: '/common/phone-number-types', parameters: {});

    return PhoneNumberType.getPhoneNumberTypefromJson(json);
  }

  Future <Commons> getCommons(String assignedCityId) async {

    final List<IdentificationType> identificationTypesList= await getIdentificationTypes();
    final List<Gender> genderList = await getGenders();
    final List<MaritalStatus> maritalStatus = await getMaritalStatus();
    final List<HomeType> homeTypes = await getHomeTypes();
    final Zone zone = await getZone(assignedCityId);
    final List<District> districts = await getDistricts(zone);

    return Commons(
        identificationTypeId: identificationTypesList,
        genders: genderList,
        maritalStatus: maritalStatus,
        homeTypes: homeTypes,
        personTypes: PersonType.getPersonTypes(),
        districts: districts
    );

  }

}
