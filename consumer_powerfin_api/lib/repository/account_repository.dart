import 'package:consumer_powerfin_api/client/api_provider.dart';
import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';
import 'package:consumer_powerfin_api/model/account_detail.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AccountRepository {
  final Dio dio;

  AccountRepository(this.dio);

  //Traer listado de direcciones por persona

  Future<List<Account>> getAccounts(Parameters parameters) async {
    try {
      var response = await ApiProvider.getData(
          filter: '/account/accounts',
          dio: this.dio,
          parameters: parameters.toJson());
      return Account.getSharedAccountsFromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<List<AccountDetail>> getDetailAccount(String accountId,
      {Parameters parameters}) async {
    try {
      var response = await ApiProvider.getData(
          filter: '/account/$accountId/details',
          dio: this.dio,
          parameters: parameters.toJson());
      return AccountDetail.getItemList(response);
    } catch (e) {
      throw e;
    }
  }

  Future<void> putDetailAccount(String accountId, AccountDetail detail) async {
    try {
      var json =  await ApiProvider.putData(
          filter: '/account/$accountId/details',
          dio: this.dio,
          data: detail.toJson());

      print(json);
    } catch (e) {
      throw e;
    }
  }
}
