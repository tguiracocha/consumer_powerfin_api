import 'package:consumer_powerfin_api/model/account.dart';
import 'package:consumer_powerfin_api/model/operator_external_service.dart';
import 'package:dio/dio.dart';

import '../client/api_provider.dart';
import '../consumer_powerfin_api.dart';

class ExternalServiceRepository {
  final Dio dio;

  ExternalServiceRepository(this.dio);

  Future<List<ExternalServiceByOperator>> getExternalServiceByOperator() async {
    try{
      var json = await ApiProvider.getData(
          dio: this.dio, filter: '/persons/external-services', parameters: {});

      return ExternalServiceByOperator.getExternalServiceByOperator(json);
    }catch (e){
      throw e;
    }
  }

  Future<String> postExternalServiceByOperator(
      ExternalService externalService, Account account, String balance, String number) async {
    try{
      var json = await ApiProvider.postData(
          dio: this.dio,
          filter: '/external-service',
          data: externalService.toJson(account, number,balance));

      return json[0]['transactionVoucher'];
    }catch (e){
      throw e;
    }
  }
}
