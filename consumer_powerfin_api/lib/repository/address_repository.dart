
import 'package:consumer_powerfin_api/client/api_provider.dart';
import 'package:consumer_powerfin_api/model/address.dart';
import 'package:dio/dio.dart';

///=====================================================================
///Repositorio para consumir los WebServices de dirección de una persona
///=====================================================================


class AddressRepository
{

  final Dio dio;

  AddressRepository(this.dio);

  //Traer listado de direcciones por persona

  Future<List<Address>> getAddress(String token, String personId) async
  {
    var response = await ApiProvider.getData(
        filter:'/persons/$personId/address',
        dio: this.dio,
        parameters: {});

    return Address.getAddressListFromJson(response);


  }

  //Agregar nueva direccion por persona

  Future<String> postAddress(String token, Address address , String personId) async
  {
    var response = await ApiProvider.postData(
        filter:'/persons/address',
        dio: this.dio,
        data: address.toJson(personId));

    return response[0]['personAddressId'];


  }

  //Modificar direccion de una persona

  Future <void> putAddress(String token, String personId, Address address) async {
    try{

      await ApiProvider.putData(
          filter:'/persons/address',
          dio:this.dio,
          data: address.toJson(personId));

    }catch (e){
      throw e;
    }
  }

}