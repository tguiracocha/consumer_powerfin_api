import 'package:consumer_powerfin_api/client/api_provider.dart';
import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';
import 'package:consumer_powerfin_api/model/fileModel.dart';
import 'package:dio/dio.dart';

class FileRepository {
  final Dio dio;

  FileRepository(this.dio);

  Future<FileModel> getReport(Parameters parameters) async {
    try {
      const REPORT_ID = 'PAYMENT_RECEIPT_APP';

      var json = await ApiProvider.getData(
          filter: '/files/report',
          dio: this.dio,
          parameters: parameters.toJson());
      return FileModel.fromJson(json[0]);

    } catch (e) {
      throw e;
    }
  }
}
