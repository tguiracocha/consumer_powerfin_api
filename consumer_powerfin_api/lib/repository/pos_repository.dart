import 'package:consumer_powerfin_api/client/api_provider.dart';
import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';
import 'package:dio/dio.dart';

class PosRepository {
  final Dio dio;

  PosRepository(this.dio);

  //Traer listado de direcciones por persona

  Future<List<Branch>> getPosData(String posId) async {
    try {
      var response =
          await ApiProvider.getData(filter: '/pos/$posId', dio: this.dio, parameters: {});
      return Branch.getBranchList(response);
    } catch (e) {
      throw e;
    }
  }
}
