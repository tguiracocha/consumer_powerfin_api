

import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';

Future<bool> initConnectivity() async {

  final Connectivity _connectivity = new Connectivity();

  String connectionStatus;

  try {
    connectionStatus = (await _connectivity.checkConnectivity()).toString();
    if (connectionStatus == "ConnectivityResult.mobile" ||
        connectionStatus == "ConnectivityResult.wifi") {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  } on PlatformException catch (e) {
    connectionStatus = "Internet connectivity failed +$e";
    return false;
  }
}
