
class InternetServicePlan{
  final String id;
  final String name;
  final double retailPrice;
  final String remark;
  final String dueDatePromotion;

  InternetServicePlan({
    this.id,
    this.name,
    this.retailPrice,
    this.remark,
    this.dueDatePromotion});


  factory InternetServicePlan.fromJson(Map json)=>
      InternetServicePlan(
          id:json.containsKey('id')?json['id']:'',
          name: json.containsKey('name')?json['name']:'',
          retailPrice: json.containsKey('price')?json['name']:0.00,
          remark: json.containsKey('remark')?json['remark']:''
      );


  static getISPList(var json)=>List<InternetServicePlan>
      .from(json.map((i) => InternetServicePlan.fromJson(i)));



}