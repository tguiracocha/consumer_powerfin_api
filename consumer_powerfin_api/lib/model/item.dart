import 'package:flutter/foundation.dart';

class Item {
  String detailId;
  final String itemId;
  final String code;
  final String nameItem;
  double priceItem;
  final double stock;
  final bool paymentOnDelivery;
  final String paymentTypeId;
  final bool allowsNegativeStock;
  bool saveItem;
  int quantity;

  Item(
      {this.detailId,
      this.itemId,
      this.code,
      this.nameItem,
      this.priceItem,
      this.stock,
      this.paymentOnDelivery,
      this.paymentTypeId,
      this.allowsNegativeStock=false,
      this.quantity,
      this.saveItem=false});

  static List<Item> getItemList(var json, {bool isDetail = false}) =>
      List<Item>.from(json.map((i) => Item.fromJson(i, isDetail: isDetail)));

  factory Item.fromJson(Map<String, dynamic> json, {bool isDetail = false}) {
    double _quantity;

    if (isDetail) _quantity = json['cartCount'];

    return new Item(
        detailId: json.containsKey('detailId') ? json['detailId'] : '',
        itemId: json.containsKey('id')?json['id']:'',
        code:json.containsKey('code') ? json['code']:'',
        nameItem:json.containsKey('name') ? json['name']:'',
        priceItem: json.containsKey('newPrice')?double.parse(json['newPrice'].toString()):0.00,
        stock: isDetail ? 0 : json['availibilityCount'],
        quantity: isDetail ? _quantity.toInt() : 1,
        paymentOnDelivery: false,
        allowsNegativeStock: json.containsKey('allowsNegativeStock')? json['allowsNegativeStock']:false,
        saveItem: isDetail?true:false,
        paymentTypeId:
            json.containsKey('paymentTypeId') ? json['paymentTypeId'] : '');
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();

    if (detailId != '')
      data["detailId"] = this.detailId;

    data["id"] = "${this.itemId}";
    data["code"] = "${this.code}";
    data["newPrice"] = "${this.priceItem}";
    data["cartCount"] = "${this.quantity}";

    return data;
  }

  Item increaseQuantity() {
    if (this.quantity < this.stock) this.quantity++;

    return this;
  }

  Item decreaseQuantity() {
    if (this.quantity != 0) this.quantity = this.quantity - 1;

    return this;
  }

  Item changeQuantity(int quantity) {
    this.quantity = quantity;
    return this;
  }

  Item copyWith(int quantity, String detailId, bool saved)
  {
    this.quantity = quantity;
    this.detailId = detailId;
    this.saveItem = saved;
    return this;
  }

  Item changePrice(double price) {
    this.priceItem = price;
    return this;
  }

}

class Detail extends Item {
  final String detailId;

  Detail(
      {this.detailId,
      @required code,
      @required name,
      @required price,
      @required quantity})
      : super(code: code, nameItem: name, priceItem: price, quantity: quantity);

  @override
  Map<String, String> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (detailId == null) data["detailId"] = this.detailId;

    data["id"] = "${this.itemId}";
    data["code"] = "${this.code}";
    data["newPrice"] = "${this.priceItem}";
    data["cartCount"] = "${this.quantity}";

    return data;
  }
}
