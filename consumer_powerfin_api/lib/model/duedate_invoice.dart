
class DueDateInvoice{

  final int idDueDate;
  final String dueDate;

  DueDateInvoice({this.idDueDate, this.dueDate});


  List<DueDateInvoice> getDueDates(var json)
  {

    List<DueDateInvoice> dueDateList=[];

    for (int i=0; i<json.length;i++){
      DueDateInvoice _dueDateInvoice = DueDateInvoice(idDueDate: i+1, dueDate:json[i]);
      dueDateList.add(_dueDateInvoice);
    }

    return dueDateList;

  }



}