

class IdentificationType{
  String identificationTypeId;
  String name;

  IdentificationType({
    this.identificationTypeId,
    this.name});

  factory IdentificationType.fromJson(Map<String, dynamic> json)=>IdentificationType(
      identificationTypeId: json['identificationTypeId'],
      name: json.containsKey('name')?json['name']:''
    );


  static getIdTypeList(var json)=>List<IdentificationType>
      .from(json.map((i) => IdentificationType.fromJson(i)));

}