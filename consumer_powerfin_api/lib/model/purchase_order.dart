
import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';

import 'account_payment_method.dart';
import 'cart.dart';

class PurchaseOrder {
  String idInvoice;
  Branch branch;
  Person person;
  List<AccountPaymentMethod> paymentMethods;
  Cart cart;
  String remark;
  Address deliveryAddress;
  //Parametro offRoute por wc
  bool offRoute;
  int authorizeInvoice;
  String paymentTypeId;
  String dueDate;

  static const String CREDIT_PAYMENT_TYPE_ID='PPD';

  static const String UDELIVERY_PAYMENT_TYPE_ID = 'PUE';

  PurchaseOrder({
    this.branch,
    this.idInvoice,
    this.person,
    this.paymentMethods,
    this.cart,
    this.remark = '',
    this.deliveryAddress,
    this.offRoute,
    this.authorizeInvoice,
    this.paymentTypeId= CREDIT_PAYMENT_TYPE_ID,
    this.dueDate
  });



  Map<String, dynamic> toJson() {

    List<Map> items = [];

    cart.items.forEach((item) => items.add(item.toJson()));
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data["posId"]= "${cart.branch.posId}";
    data["totalAmount"]= "${this.cart.totalToPay}";
    data["remark"]= "$remark";
    data["offRoute"]= offRoute;
    data["authorizeInvoice"]= authorizeInvoice;
    data["paymentMethodList"]=[];
    if(deliveryAddress!=null)
      data["personAddressId"]=this.deliveryAddress.personAddressId;
    data["accountItemList"]= items;
    data["personId"]= "${this.person.personId}";
    if(this.dueDate ==null)
      data["paymentTypeId"]=UDELIVERY_PAYMENT_TYPE_ID;
    else
      data["paymentTypeId"]="${this.paymentTypeId}";
    data["dueDate"]=this.dueDate;


    return data;
  }

  PurchaseOrder copyWith({
    Cart cart,
    List<AccountPaymentMethod> paymentMethods,
    bool offRoute,
    bool authorizeInvoice,
    String remark = '',
  }) =>
      PurchaseOrder(
        person: this.person,
        branch: this.branch,
        cart: cart == null ? this.cart : cart,
        offRoute: offRoute,
        authorizeInvoice: authorizeInvoice ? 1 : 0,
        paymentMethods:
            paymentMethods == null ? this.paymentMethods : paymentMethods,
        remark: remark == '' ? this.remark : remark,
      );
}
