class User {
  final String username;
  final String password;
  final bool isActive;
  final String token;

  User({this.username, this.password, this.isActive, this.token});

  User.fromJson(Map<String, dynamic> json)
      : username = json['name'],
        password = json['email'],
        isActive = true,
        token = json.containsKey('token') ? json['token'] : null;

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data["username"] = "$username";
    if (password != null) data["password"] = "$password";
    if (token != null) data["token"] = token;
    return data;
  }

  User.fromJsonLocal(Map<String, dynamic> json)
      : username = json['username'],
        password = json['email'],
        isActive = true,
        token = json.containsKey('token') ? json['token'] : null;

  static getUserList(var json) =>
      List<User>.from(json.map((i) => User.fromJsonLocal(i)));

  static List<Map<String, String>> toJsonUserList(List<User> userList) =>
      List<Map<String, String>>.from(
          userList.map((User _user) => _user.toJson()));

  @override
  String toString() => "{User: $username}";
}
