import 'package:consumer_powerfin_api/model/district.dart';

class Zone {

  final District country;
  final District region;
  final District state;
  final District city;

  Zone({
    this.country,
    this.region,
    this.state,
    this.city });

  factory Zone.fromJson(var json)=>
      Zone(
          country: District(
              id:   json['country']['countryId'].toString(),
              name: json['country']['name']),
          region: District(
              id:   json['region']['regionId'].toString(),
              name: json['region']['name']),
          state:  District(
              id:   json['state']['stateId'].toString(),
              name: json['state']['name']),
          city: District(
              id:   json['cityId'].toString(),
              name: json['name'])
      );

  static Map<String, String> toJson(Zone zone) =>
      {
        "countryId" :"${zone.country.id}",
        "regionId"  :"${zone.region.id}",
        "stateId"   :"${zone.state.id}",
        "cityId"    :"${zone.city.id}"
      };

}