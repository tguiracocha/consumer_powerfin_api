
import 'account.dart';

///Servicios Externos
///FULLCARGAS

class ExternalService {
  final bool allowDecimalAmount;
  final String externalServerName;
  final String externalServiceCode;
  final String externalServiceId;
  final double maxAmount;
  final double minAmount;
  final String name;
  final String operatorName;
  final String numberFormat;

  ExternalService(
      {this.allowDecimalAmount,
        this.externalServerName,
        this.externalServiceCode,
        this.externalServiceId,
        this.maxAmount,
        this.minAmount,
        this.name,
        this.operatorName,
        this.numberFormat});

  factory ExternalService.fromJson(Map json)=>
      ExternalService(
          allowDecimalAmount: json['allowDecimalAmount']==1?true:false,
          externalServerName: json['externalServerName'],
          externalServiceCode: json['externalServiceCode'],
          externalServiceId:  json['externalServiceId'],
          maxAmount: json['maxAmount'],
          minAmount: json['minAmount'],
          name: json['name'],
          operatorName: json['operatorName'],
          numberFormat: json['numberFormat']
      );

  static getExternalServiceList(var json)=>List<ExternalService>
      .from(json.map((i) => ExternalService.fromJson(i)));

  @override
  String toString() {
    return '$externalServiceCode - $operatorName - $name';
  }

  Map<String, dynamic> toJson(Account account, String number, String balance)
  {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data["accountId"]= '${account.accountId}';
    data["externalServiceId"]= '${this.externalServiceId}';
    data["value"]= "$balance";
    data["number"]= "$number";
    return data;

  }


}
