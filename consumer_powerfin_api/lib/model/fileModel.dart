class FileModel{
  var data;
  final String dataType;
  final String id;
  final String name;

  FileModel(this.data, this.dataType, this.id, this.name);

  factory FileModel.fromJson(var json)=>FileModel(
      json['data'],
      json['dataType'],
      json['id'],
      json['name']);


}