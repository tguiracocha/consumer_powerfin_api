
import '../consumer_powerfin_api.dart';

class Commons{

  final List<IdentificationType> identificationTypeId;
  final List<Gender> genders;
  final List<MaritalStatus> maritalStatus;
  final List<HomeType> homeTypes;
  final List<PersonType> personTypes;
  final List<District> districts;
  final List<VisitType> visitTpes;
  final List<VisitResult> visitResults;

  Commons({
      this.identificationTypeId,
      this.genders,
      this.maritalStatus,
      this.homeTypes,
      this.personTypes,
      this.districts,
      this.visitResults,
      this.visitTpes
  });

}