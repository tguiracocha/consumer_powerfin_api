
class VisitType {
  final String visitTypeId;
  final String visitTypeName;

  VisitType({this.visitTypeId, this.visitTypeName});

  factory VisitType.fromJson(var json) {

    return VisitType(
      visitTypeId: json.containsKey('visitTypeId')?json['visitTypeId']:null,
      visitTypeName: json.containsKey('name')?json['name']:null,
    );
  }

  static List<VisitType> getVisitTypeList(var json) =>List<VisitType>
      .from(json.map((i) => VisitType.fromJson(i)));


}
