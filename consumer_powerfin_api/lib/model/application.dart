import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';

class Application {
  String _applicationId;
  String _packageId;
  String _baseUrl;
  String _company;
  int _multiUser;
  int _multiCompany;
  String _apiKey;
  List<User> _userList;

  Application(
      {String applicationId,
      String packageId,
      String baseUrl,
      String company,
      int multiUser,
      int multiCompany,
      String apiKey,
      List<User> userList}) {
    _applicationId = applicationId;
    _packageId = packageId;
    _baseUrl = baseUrl;
    _company = company;
    _multiUser = multiUser;
    _multiCompany = multiCompany;
    _userList = userList;
    _apiKey = apiKey;
  }

  set setUser(User user) {
    if (_multiUser == 1) {
      int index = userList.indexWhere((u) => u.username == user.username);
      if (index == -1) {
        _userList.add(user);
      } else {
        _userList[index] = user;
      }
    } else {
      _userList = [user];
    }
  }

  removeUser(User user) {
    if (_multiUser == 1) {
      int index = userList.indexWhere((u) => u.username == user.username);
      if (index != -1) {
        _userList.removeAt(index);
      }
    } else {
      _userList = [];
    }

  }

  Application.fromJson(dynamic json) {
    _applicationId = json['application_id'];
    _packageId = json['package_id'];
    _baseUrl = json['base_url'];
    _company = json['company'];
    _multiUser = json['multi_user'];
    _multiCompany = json['multi_company'];
    _apiKey = json.containsKey('apikey') ? json['apikey'] : '';
    _userList = json.containsKey('user') ? User.getUserList(json['user']) : [];
  }

  Application copyWith(
          {String applicationId,
          String packageId,
          String baseUrl,
          String company,
          int multiUser,
          int multiCompany,
          List<User> userList}) =>
      Application(
          applicationId:
              applicationId != null ? _applicationId : this._applicationId,
          packageId: packageId != null ? packageId : this._packageId,
          baseUrl: baseUrl != null ? baseUrl : this._baseUrl,
          company: company != null ? company : this._company,
          multiUser: multiUser != null ? multiUser : this._multiUser,
          multiCompany:
              multiCompany != null ? multiCompany : this._multiCompany,
          userList: userList.isNotEmpty ? userList : this._userList);

  String get applicationId => _applicationId;
  String get packageId => _packageId;
  String get baseUrl => _baseUrl;
  String get company => _company;
  int get multiUser => _multiUser;
  int get multiCompany => _multiCompany;
  String get apiKey => _apiKey;
  List<User> get userList => _userList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['application_id'] = _applicationId;
    map['package_id'] = _packageId;
    map['base_url'] = _baseUrl;
    map['company'] = _company;
    map['multi_user'] = _multiUser;
    map['multi_company'] = _multiCompany;
    map['apikey'] = _apiKey;
    map['user'] = User.toJsonUserList(_userList);
    return map;
  }

  Map<String, dynamic> toJsonParameter() {
    final map = <String, dynamic>{};
    map['application_id'] = _applicationId;
    map['package_id'] = _packageId;
    map['base_url'] = _baseUrl;
    map['company'] = _company;
    map['multi_user'] = _multiUser;
    map['multi_company'] = _multiCompany;
    map['apikey'] = _apiKey;
    return map;
  }
}
