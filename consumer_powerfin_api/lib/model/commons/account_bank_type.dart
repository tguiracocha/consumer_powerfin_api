
class AccountBankType {
  final String accountBankTypeId;
  final String name;

  AccountBankType({this.accountBankTypeId, this.name});

  AccountBankType.fromJson(Map<String, dynamic> json)
      : accountBankTypeId = json['accountBankTypeId'],
        name = json['name'] {
    print('In AccountBankType.fromJson(): ($name)');
  }

  static getAccountBankTypeList(var json) => List<AccountBankType>.from(
      json.map((type) => AccountBankType.fromJson(type)));
}
