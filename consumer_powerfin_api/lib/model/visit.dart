import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';
import 'package:geolocator/geolocator.dart';

class Visit {
  String visitId;
  final String dateTimeBegin;
  final String dateTimeEnd;
  final Person person;
  final String remark;
  final VisitType visitType;
  final List<VisitResult> visitResult;
  final Position positionBegin;
  final Position positionEnd;
  final String userId;
  final bool canFinishVisit;
  final Address address;

  Visit(
      {this.visitId,
      this.visitType,
      this.visitResult,
      this.dateTimeBegin,
      this.dateTimeEnd,
      this.person,
      this.positionBegin,
      this.positionEnd,
      this.userId,
      this.remark,
      this.canFinishVisit = false,
      this.address});

  factory Visit.fromJson(Map<String, dynamic> json) => Visit(
      visitId: json.containsKey("visitId") ? json["visitId"] : '',
      remark: json.containsKey("remark") ? json["remark"] : '',
      visitType: VisitType(
        visitTypeName:
            json.containsKey("visitTypeName") ? json["visitTypeName"] : null,
        visitTypeId:
            json.containsKey("visitTypeId") ? json["visitTypeId"] : null,
      ),
      dateTimeBegin: json.containsKey("beginDate") ? json["beginDate"] : null,
      dateTimeEnd: json.containsKey("endDate") ? json["endDate"] : null,
      person: Person(
        personId: json.containsKey("personId") ? json["personId"] : 0,
        name: json.containsKey("personName") ? json["personName"] : '',
      ),
      canFinishVisit: json.containsKey("endDate")?false:true);

  static bool validateVisitInProgress(String initDate, DateTime endDate) {
    Duration diff = endDate.difference(DateTime.parse(initDate));
    int minuts = diff.inMinutes;
    if (minuts < 30) {
      return true;
    } else {
      return false;
    }
  }

  static List<Visit> getVisitList(var json) =>
      List<Visit>.from(json.map((i) => Visit.fromJson(i)));

  static List<Visit> getPendingVisitList(var json) {
    return List<Visit>.from(json.map((i) {
      if (!i.containsKey('endDate')) {
        return Visit.fromJson(i);
      }
    }));
  }

  Map<String, dynamic> toJsonBegin() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['beginDate'] = this.dateTimeBegin;
    data['beginLatitude'] = this.positionBegin.latitude;
    data['beginLongitude'] = this.positionBegin.longitude;
    data['userName'] = "${this.userId}";
    data['visitTypeId'] = "${this.visitType.visitTypeId}";

    return data;
  }

  Map<String, dynamic> toJsonEnd() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['endDate'] = this.dateTimeEnd;
    data['endLatitude'] = this.positionEnd.latitude;
    data['endLongitude'] = this.positionEnd.longitude;
    if (this.person != null) if (this.person.personId != null)
      data['personId'] = this.person != null ? '${this.person.personId}' : "";
    data['remark'] = "${this.remark}";
    data['visitId'] = "${this.visitId}";
    data['results'] = VisitResult().toJson(this.visitResult);
    if(address!=null)
      data['personAddressId']=this.address.personAddressId;
    print(data);
    return data;
  }

  Visit copyWith(
          {String userId,
          String visitId,
          Position positionBegin,
          Position positionEnd,
          String dateTimeEnd,
          List<VisitResult> result,
          String remark,
          Person person,
          Address address}) =>
      Visit(
          visitId: visitId != null ? visitId : this.visitId,
          visitType: this.visitType,
          visitResult: result != null ? result : this.visitResult,
          dateTimeBegin: this.dateTimeBegin,
          dateTimeEnd:
              dateTimeEnd != null ? dateTimeEnd : DateTime.now().toString(),
          person: person != null ? person : this.person,
          positionBegin: positionBegin == null ? Position() : positionBegin,
          positionEnd: positionEnd == null ? Position() : positionEnd,
          userId: userId != null ? userId : null,
          remark: remark != null ? remark : this.remark,
          address: address!=null? address: this.address);
}
