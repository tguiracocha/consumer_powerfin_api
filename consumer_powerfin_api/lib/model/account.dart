//Cuentas - (EWALLET/BONO/CRÉDITO)

import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';

class Account {
  final String accountId;
  final String code;
  final double advance;
  final double balance;
  final double balaceBlocked;
  final int personId;
  final String name;
  final String openingDate;
  final String statusName;
  final String branchId;
  final String branchName;
  List<AccountDetail> detail;
  double balancetoUse;

  Account(
      {this.accountId,
      this.code,
      this.advance,
      this.balance,
      this.balaceBlocked,
      this.personId,
      this.name,
      this.openingDate,
      this.statusName,
      this.branchId,
      this.branchName,
      this.detail,
      this.balancetoUse});

  Account.fromJson(Map<String, dynamic> json)
      : accountId = json['accountId'],
        balance = json.containsKey('balance')
            ? double.parse("${json['balance']}")
            : 0.00,
        code = json.containsKey('code') ? json['code'] : '',
        advance = json.containsKey('advance')
            ? double.parse("${json['advance']}")
            : 0.00,
        balaceBlocked = json.containsKey('balanceBlocked')
            ? double.parse("${json['balanceBlocked']}")
            : 0.00,
        personId = 0,
        name = json['name'],
        openingDate = json['openingDateAsString'],
        statusName = json['statusName'],
        branchId = json.containsKey('branchId') ? json['branchId'] : '',
        branchName = json.containsKey('branchName') ? json['branchName'] : '',
        detail = json.containsKey('detail')
            ? AccountDetail.fromJson(json['accountDetail'])
            : [] {
    print('In Account.fromJson: $accountId - $name');
  }

  static List<Account> getSharedAccountsFromJson(var json) =>
      List<Account>.from(json.map((_account) => Account.fromJson(_account)));

  @override
  String toString() {
    return '$accountId - $statusName';
  }

  int calculateTotalReviewItems() {
    int total = 0;

    if (this.detail.length != 0) {
      total = this
          .detail
          .map((item) => item.reviewFinalized ? 1 : 0)
          .fold(0, (previous, next) => previous + next);
    }

    return total;
  }
}
