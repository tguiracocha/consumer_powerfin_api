import 'package:flutter/material.dart';

import 'person/person.dart';
import 'external_service.dart';

class ExternalServiceByOperator extends Person {

  final List<ExternalService> externalService;
  final String logoUrl;


  ExternalServiceByOperator(
      { @required personId,
        @required name,
        @required identification,
      this.externalService, this.logoUrl})
      : super(personId: personId, name: name, identification: identification);

  factory ExternalServiceByOperator.fromJson(Map json){

    ExternalServiceByOperator externalServiceByOperator =ExternalServiceByOperator(
        personId: json['personId'],
        name: json['name'],
        identification : json['identification'],
        logoUrl:json['logoUrl'],
        externalService: ExternalService.getExternalServiceList(json['externalService']));

    print('InExternalServiceByOperator:' + externalServiceByOperator.name);

    return externalServiceByOperator;
  }

  static getExternalServiceByOperator(var json)=>List<ExternalServiceByOperator>
      .from(json.map((i) => ExternalServiceByOperator.fromJson(i)));



}
