import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';

import 'payment_type_invoice.dart';
import 'item.dart';

class SaleOrderDetail {
  List<Item> items;
  int totalItems = 0;
  double totalToPay = 0.00;
  double subtotalPaymentOnDelivery = 0.00;
  double subtotalCredit = 0.00;

  SaleOrderDetail({this.items});

  static const String CREDIT_PAYMENT_TYPE_ID = 'PPD';

  static final SaleOrderDetail detail = SaleOrderDetail(items: []);

  // factory constructor
  factory SaleOrderDetail.create() {
    return detail;
  }

  static SaleOrderDetail copyWith(
      List<Item> details, PaymentTypeInvoice paymentTypeInvoice) {

    SaleOrderDetail saleOrderDetail = SaleOrderDetail(items: details);

    saleOrderDetail.calculateTotalProducts();
    saleOrderDetail.calculateTotalToPay(paymentTypeInvoice);

    return saleOrderDetail;
  }

  SaleOrderDetail addProduct(
      Item product, PaymentTypeInvoice paymentTypeInvoice) {
    int index = items.indexWhere((item) => item.itemId == product.itemId);
    int quantity = 1;
    if (index != -1) quantity++;

    bool allowPurchase = validateStock(product, quantity);

    if (allowPurchase && index == -1) {
      items.add(product);
    } else if (allowPurchase && index != -1) {
      items[index].increaseQuantity();
    } else {
      throw ('Sin Stock');
    }

    calculateTotalToPay(paymentTypeInvoice);
    calculateTotalProducts();

    return this;
  }

  SaleOrderDetail removeProduct(
      Item product, PaymentTypeInvoice paymentTypeInvoice) {
    int index = items.indexWhere((item) => item.itemId == product.itemId);
    items.removeAt(index);
    calculateTotalToPay(paymentTypeInvoice);
    calculateTotalProducts();

    return this;
  }

  SaleOrderDetail increaseQuantity(
      Item product, PaymentTypeInvoice paymentTypeInvoice) {
    final bool allowSale = validateStock(product, product.quantity + 1);

    if (allowSale) {
      int index = items.indexWhere((item) => item.itemId == product.itemId);
      items[index].increaseQuantity();
      calculateTotalToPay(paymentTypeInvoice);
      calculateTotalProducts();
    }else
      {
        throw ('SIN STOCK');
      }

    return this;
  }

  SaleOrderDetail decreaseQuantity(
      Item product, PaymentTypeInvoice paymentTypeInvoice) {
    int index = items.indexWhere((item) => item.itemId == product.itemId);
    items[index].decreaseQuantity();
    calculateTotalToPay(paymentTypeInvoice);
    calculateTotalProducts();
    return this;
  }

  SaleOrderDetail changeQuantity(
      Item product, int quantity, PaymentTypeInvoice paymentTypeInvoice) {
    final bool allowSale = validateStock(product, quantity);

    if (allowSale) {
      int index = items.indexWhere((item) => item.itemId == product.itemId);
      items[index].changeQuantity(quantity);
      calculateTotalProducts();
      calculateTotalToPay(paymentTypeInvoice);
    } else {
      throw ('SIN STOCK');
    }

    return this;
  }

  SaleOrderDetail calculateTotalProducts() {
    if (this.items.length != 0) {
      this.totalItems = this
          .items
          .map((product) => product.quantity)
          .fold(0, (previous, next) => previous + next);
    }

    return this;
  }

  bool validateStock(Item item, int quantity) {
    bool allowSale = true;

    if (!item.allowsNegativeStock) {
      if (quantity > 0 && item.stock != 0 && quantity <= item.stock)
        allowSale = true;
      else
        allowSale=false;
    }

    return allowSale;

  }

  ///===================================================================
  ///Calcular el total a pagar de acuerdo al tipo de pago de la factura
  ///y el tipo de pago del item (PAGO CONTRA ENTREGA Y CRÉDITO).
  ///Existen items que no se pueden vender a crédito El pago es
  ///contra-entrega.
  ///===================================================================

  SaleOrderDetail calculateTotalToPay(PaymentTypeInvoice paymentTypeInvoice) {
    if (paymentTypeInvoice.paymentTypeId == CREDIT_PAYMENT_TYPE_ID) {
      this.subtotalPaymentOnDelivery = items
          .map((Item item) => (item.paymentTypeId != CREDIT_PAYMENT_TYPE_ID)
              ? item.priceItem * item.quantity
              : 0.00)
          .fold(0.0, (previous, next) => previous + next);

      this.subtotalCredit = items
          .map((item) => (item.paymentTypeId == CREDIT_PAYMENT_TYPE_ID)
              ? item.priceItem * item.quantity
              : 0.00)
          .fold(0.0, (previous, next) => previous + next);
    } else {
      this.subtotalPaymentOnDelivery = items
          .map((item) => item.priceItem * item.quantity)
          .fold(0.0, (previous, next) => previous + next);
    }

    this.totalToPay = subtotalCredit + subtotalPaymentOnDelivery;

    return this;
  }

  SaleOrderDetail changePrice(

      Item product, double price, PaymentTypeInvoice paymentTypeInvoice) {
    int index = items.indexWhere((item) => item.itemId == product.itemId);
    items[index].changePrice(price);
    calculateTotalToPay(paymentTypeInvoice);

    return this;
  }

}
