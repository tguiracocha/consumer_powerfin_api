import 'package:consumer_powerfin_api/model/home_types.dart';
import 'package:consumer_powerfin_api/model/district.dart';
import 'package:geolocator/geolocator.dart';

class Address {
  final String personAddressId;
  final String homeDistrictId;
  final HomeType homeType;
  final bool isDefault;
  final String homeMainStreet;
  final String homeSideStreet;
  final String homeNumber;
  final District district;
  final String reference;
  final Position position;
  final String personAddressType;
  var addressPhoto;

  Address(
      {this.homeDistrictId,
      this.homeType,
      this.isDefault,
      this.personAddressId,
      this.homeMainStreet,
      this.homeSideStreet,
      this.homeNumber,
      this.district,
      this.reference,
      this.position,
      this.personAddressType,
      this.addressPhoto});

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        homeDistrictId: json.containsKey('homeDistrictId')
            ? "${json['homeDistrictId']}"
            : '',
        district: json.containsKey('disctrictBean')
            ? District.fromJson(json['disctrictBean'])
            : District(),
        homeType: HomeType(
            homeTypeId:
                json.containsKey('homeTypeId') ? '${json['homeTypeId']}' : ''),
        isDefault: json.containsKey('isDefault')
            ? json['isDefault'] == 0
                ? false
                : true
            : false,
        homeMainStreet:
            json.containsKey('mainStreet') ? json['mainStreet'] : '',
        personAddressId:
            json.containsKey('personAddressId') ? json['personAddressId'] : '',
        personAddressType: json.containsKey('personAddressTypeId')
            ? json['personAddressTypeId']
            : '',
        reference: json.containsKey('reference')?json['reference']:'',
        homeNumber:
            json.containsKey('propertyNumber') ? json['propertyNumber'] : '',
        homeSideStreet:
            json.containsKey('secundaryStreet') ? json['secundaryStreet'] : '',
        position: Position(
            latitude: json.containsKey('latitude') ? json['latitude'] : 0,
            longitude: json.containsKey('longitude') ? json['longitude'] : 0),
      );

  static List<Address> getAddressListFromJson(var json) =>
      List<Address>.from(json.map((i) => Address.fromJson(i)));

  @override
  String toString() {
    return '$homeMainStreet - $homeNumber $homeSideStreet';
  }

  Map<String, dynamic> toJson(String personId) {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data["personId"] = '$personId';
    data["disctrictBean"] = this.district.toJson();
    data["homeTypeId"] = "${this.homeType.homeTypeId}";
    data["latitude"] = '${this.position.latitude}';
    data["longitude"] = '${this.position.longitude}';
    data["mainStreet"] = this.homeMainStreet;
    data["reference"] = this.reference;

    if (this.isDefault != null) data["isDefault"] = this.isDefault ? 1 : 0;

    if (this.personAddressId != null)
      data["personAddressId"] = this.personAddressId;

    if (this.personAddressType != null)
      data["personAddressTypeId"] = this.personAddressType;

    if (this.homeNumber != null) data["propertyNumber"] = '${this.homeNumber}';

    if (this.homeSideStreet != null)
      data["secundaryStreet"] = this.homeSideStreet;

    if (this.addressPhoto != null) data["addressPhotoData"] = this.addressPhoto;

    print('Address: ' + data.toString());
    return data;
  }

  Address copyWith(
          {String homeMainStreet,
          String homeSideStreet,
          String homeNumber,
          String remark,
          Position position,
          String personAddressId,
          var addressPhoto}) =>
      Address(
          homeDistrictId: this.homeDistrictId,
          homeType: this.homeType,
          district: this.district,
          isDefault: this.isDefault,
          homeMainStreet:
              homeMainStreet == null ? this.homeMainStreet : homeMainStreet,
          homeSideStreet:
              homeSideStreet == null ? this.homeSideStreet : homeSideStreet,
          homeNumber: homeNumber == null ? this.homeNumber : homeNumber,
          reference: remark == null ? this.reference : remark,
          position: position == null ? this.position : position,
          personAddressId:
              personAddressId == null ? this.personAddressId : personAddressId,
          personAddressType: this.personAddressType,
          addressPhoto:
              addressPhoto == null ? this.addressPhoto : addressPhoto);
}
