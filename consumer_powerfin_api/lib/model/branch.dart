
class Branch
{
  final int branchId;
  final String branchName;
  final String posId;
  final String posName;
  final String branchAddress;

  Branch({
    this.branchId,
    this.branchName,
    this.posId,
    this.posName,
    this.branchAddress
  });


  Branch.fromJson(Map <String, dynamic> json)
      : branchId= json['branchId'],
        branchName= json['branchName'],
        posId= json['posId'],
        posName= json['posName'],
        branchAddress = json['branchAddress']{
    print('In Branch.fromJson: {$branchName $posName}');
  }

  static getBranchList(var json)=>List<Branch>
      .from(json.map((i) => Branch.fromJson(i)));

  @override
  String toString() {
    return '$branchName - $posName';
  }


}