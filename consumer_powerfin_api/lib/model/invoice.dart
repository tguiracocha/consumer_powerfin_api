
import 'account_payment_method.dart';
import 'branch.dart';

class Invoice {

  final String id;
  final String code;
  final String collectionBox;
  final double balance;
  final String issueDate;
  final String name;
  final Branch store;
  double totalCollected;
  List<AccountPaymentMethod> paymentMethods;
  bool selected;

  Invoice(
      {this.code,
      this.collectionBox,
      this.id,
      this.balance,
      this.issueDate,
      this.name,
      this.paymentMethods,
      this.store,
      this.totalCollected,
      this.selected = false});

  factory Invoice.fromJson(var json) {

    String _date = json.containsKey('issueDate') ? json['issueDate'] : '';
    int position = _date.indexOf('T');

    Branch _store = Branch.fromJson(json);

    Invoice invoice = new Invoice(
        code: json.containsKey('code') ? json['code'] : '',
        collectionBox:
            json.containsKey('collectionBox') ? json['collectionBox'] : '',
        id: json.containsKey('id') ? json['id'] : '',
        balance: json.containsKey('balance') ? json['balance'] : '',
        issueDate: _date.substring(0, position),
        name: json.containsKey('name') ? json['name'] : '',
        paymentMethods:[],
        store: _store,
        selected: false);

    return invoice;
  }

  Map<String, dynamic> toJson() {

    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['accountId'] = this.id;
    if (this.paymentMethods != null)
      data['paymentMethodList'] = this.paymentMethods.map((AccountPaymentMethod v) => v.toJsonInvoice()).toList();

    return data;

  }

  static List<Invoice> getInvoiceList(var json)=>List<Invoice>
      .from(json.map((i) => Invoice.fromJson(i)));


  Invoice copyWith(AccountPaymentMethod accountPaymentMethod) {
    this.paymentMethods = [accountPaymentMethod];
    return this;
  }


  Invoice addPaymentMethod(AccountPaymentMethod accountPaymentMethod){
    this.paymentMethods.add(accountPaymentMethod);
    this.totalCollectedByPaymentMethod();
    return this;
  }

  Invoice removePaymentMethod(Invoice invoice, AccountPaymentMethod paymentMethod) {


    int index = invoice.paymentMethods.indexWhere(
            (AccountPaymentMethod _accountPaymentMethod) =>
        _accountPaymentMethod.dateTime == paymentMethod.dateTime);

    if (index != -1)
      invoice.paymentMethods.removeAt(index);


    invoice.totalCollectedByPaymentMethod();

    return invoice;

  }

  //Payment Receipt
  List<Invoice> addDetail(List<Invoice> invoiceList, Invoice invoice) {

    int index =
    invoiceList.indexWhere((Invoice _invoice) => invoice.id == _invoice.id);

    if (index == -1) {
      invoiceList.add(invoice);
    } else {
      invoiceList[index].addPaymentMethod(invoice.paymentMethods[0]);
      invoiceList[index].totalCollectedByPaymentMethod();
    }

    return invoiceList;
  }

  List<Invoice> removeDetail(List<Invoice> invoiceList,Invoice invoice) {

    int index =
    invoiceList.indexWhere((Invoice _invoice) => invoice.id == _invoice.id);
    invoiceList.removeAt(index);
    //  this.calculateTotalCollected();

    return invoiceList;
  }

  Invoice totalCollectedByPaymentMethod() {

    this.totalCollected = paymentMethods
        .map((item) => item.paymentMethodBalance)
        .fold(0.0, (previous, next) => previous + next);

    return this;
  }

}
