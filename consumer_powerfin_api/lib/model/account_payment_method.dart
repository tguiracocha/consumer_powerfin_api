import 'package:consumer_powerfin_api/model/payment_method.dart';
import 'commons/account_bank_type.dart';
import 'financial_entity.dart';

class AccountPaymentMethod {

  final DateTime dateTime;
  final PaymentMethod paymentMethod;
  final double paymentMethodBalance;
  final FinancialEntity financialEntity;
  final String documentNumber;
//  final String iconData;
  bool isSelected;
  double balanceToUse;
  final String idHolder;
  final String nameHolder;
  final AccountBankType accountBankType;

  AccountPaymentMethod({
    this.dateTime,
    this.paymentMethod,
    this.paymentMethodBalance = 0,
    this.financialEntity,
    this.documentNumber,
  //  this.iconData,
    this.isSelected,
    this.balanceToUse = 0,
    this.idHolder,
    this.nameHolder,
    this.accountBankType
  });

  AccountPaymentMethod copywith(
      {bool isSelect = false, double balance = 0.00}) {
    AccountPaymentMethod paymentMethod = new AccountPaymentMethod(
        paymentMethod: this.paymentMethod,
        paymentMethodBalance: this.paymentMethodBalance,
       // iconData: this.iconData,
        isSelected: isSelect == false ? this.isSelected : isSelect,
        balanceToUse: balance == null ? this.balanceToUse : balance);

    return paymentMethod;
  }

  factory AccountPaymentMethod.fromJson(var json,
      {bool isQueryInvoice = false}) {

    return AccountPaymentMethod(
        paymentMethod: PaymentMethod(
          paymentMethodId: json['paymentMethodId'],
          paymentMethodName: json['name'],
        ),
        paymentMethodBalance: isQueryInvoice
            ? 0.00
            : json.containsKey('balance')
                ? double.parse(json['balance'].toString())
                : 0.00,
        //iconData: _iconData,
        isSelected: false,
        balanceToUse: isQueryInvoice ? json['amount'] : 0.00);
  }


  Map<String, String> toJsonInvoice() {

    final Map<String, String> data = new Map<String, String>();
    data['paymentMethodId'] = this.paymentMethod.paymentMethodId;
    data['financialEntityId']= this.financialEntity!=null? this.financialEntity.id:null;
    data['amount'] = '${this.paymentMethodBalance}';
    data['voucher'] = this.documentNumber;
    return data;

  }

  Map<String, String> toJsonISP() {

    final Map<String, String> data = new Map<String, String>();

    data['paymentMethodId'] = this.paymentMethod.paymentMethodId;

    if(this.financialEntity!=null)
      data['financialEntityId']=  this.financialEntity.id;

    if(this.documentNumber!=null)
      data['accountNumber'] = '${this.documentNumber}';

    if(this.dateTime!=null)
      data['expirationDate'] = "${this.dateTime}";

    if(this.accountBankType!=null)
      data['accountBankTypeId']= this.accountBankType.accountBankTypeId;

    if(this.nameHolder!=null)
      data['holderName']=this.nameHolder;

    if(this.idHolder!=null)
      data['holderIdentification'] = this.idHolder;
    return data;

  }

}

class ListAccountPaymentMethods {
  double totalAvailable = 0.00;
  List<AccountPaymentMethod> paymentMethods;

  ListAccountPaymentMethods({
    this.totalAvailable,
    this.paymentMethods,
  });


  factory ListAccountPaymentMethods.fromJson(var json) {

    List<AccountPaymentMethod> _paymentMethods = List<AccountPaymentMethod>
        .from(json.map((i) => AccountPaymentMethod.fromJson(i)));

    ListAccountPaymentMethods paymentMethods =
        new ListAccountPaymentMethods(paymentMethods: _paymentMethods);

    return paymentMethods.calculateTotalToPay();
  }

  ListAccountPaymentMethods calculateTotalToPay() {

    if (paymentMethods.length != 0) {
      this.totalAvailable = paymentMethods
          .map((paymentMethod) => paymentMethod.paymentMethodBalance)
          .fold(0.0, (previous, next) => previous + next);
    }

    return this;
  }

}
