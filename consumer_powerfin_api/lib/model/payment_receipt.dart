
import 'package:consumer_powerfin_api/model/person/person.dart';

import 'invoice.dart';

class PaymentReceipt {

  String accountId;
  String codeReceipt;
  Person person;
  List<Invoice> invoiceList = [];
  double totalCollected;

  PaymentReceipt(
      {this.accountId,
      this.codeReceipt,
      this.person,
      this.invoiceList,
      this.totalCollected = 0.00});

  Map<String, dynamic> toJson() {

    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['personId'] = this.person.personId;
    data['code'] = this.codeReceipt;

    if (this.invoiceList != null) {
      data['accountList'] =
          this.invoiceList.map((Invoice v) => v.toJson()).toList();
    }

    print(data);
    return data;
  }


  double calculateTotalCollected(List<Invoice> invoiceList) {
    double _totalCollected = invoiceList
        .map((Invoice invoice) => invoice.totalCollected)
        .fold(0.00, (previous, next) => previous + next);

    return _totalCollected;
  }
}

class PaymenReceiptSummary {
  final String name;
  final int quantity;
  final double value;

  PaymenReceiptSummary( this.name, this.quantity, this.value);

  factory PaymenReceiptSummary.fromJson(var json) {
    return PaymenReceiptSummary(
      json['name'],
      json['quantity'],
      json['amount']
    );
  }

  static List<PaymenReceiptSummary> getPaymentReceiptSummaryFromJson(var json) {
    List<PaymenReceiptSummary> paymentReceiptSummaryList = [];

    if (json.length != 0) {
      for (var i = 0; i < json.length; i++) {
        PaymenReceiptSummary _paymentReceipt =
            PaymenReceiptSummary.fromJson(json[i]);
        if (_paymentReceipt.name != null)
          paymentReceiptSummaryList.add(_paymentReceipt);
      }
    }

    return paymentReceiptSummaryList;
  }
}
