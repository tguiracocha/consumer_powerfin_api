class Parameters {
  final String accountId;
  final String fromDate;
  final String toDate;
  final String personId;
  final String branchId;
  final String locationId;
  final String categoryId;
  final String reportId;
  final String userId;
  final String accountStatusId;
  final String name;
  final String personStatusId;
  final String identification;
  final String code;
  final String productId;
  final String productTypeId;
  final int p;
  final int l;

  Parameters(this.reportId,
      {this.accountId,
      this.fromDate,
      this.toDate,
      this.personId,
      this.branchId,
      this.locationId,
      this.categoryId,
      this.userId,
      this.accountStatusId,
      this.name,
      this.personStatusId,
      this.identification,
      this.code,
      this.productId,
      this.productTypeId,
      this.p,
      this.l});

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['reportId'] = "${this.reportId}";

    if (accountId != null) data['accountId'] = "${this.accountId}";
    if (fromDate != null) data['fromDate'] = "${this.fromDate}";
    if (toDate != null) data['toDate'] = "${this.toDate}";
    if (personId != null) data['personId'] = "${this.personId}";
    if (branchId != null) data['branchId'] = "${this.branchId}";
    if (locationId != null) data['locationId'] = "${this.locationId}";
    if (categoryId != null) data['categoryId'] = "${this.categoryId}";
    if (accountStatusId != null)
      data['accountStatusId'] = "${this.accountStatusId}";
    if (name != null) data['name'] = "${this.name}";
    if (personStatusId != null)
      data['personStatusId'] = "${this.personStatusId}";
    if (identification != null)
      data['identification'] = "${this.identification}";

    if (userId != null) {
      data['userId'] = "${this.userId}";
      data['user'] = "${this.userId}";
    }

    if (code != null) data['code'] = "${this.code}";
    if (productTypeId != null) data['productTypeId'] = "${this.productTypeId}";
    if (productId != null) data['productId'] = "${this.productId}";
    if(p!=null) data['p']='$p';
    if(l!=null) data['l']='$l';

    return data;
  }
}
