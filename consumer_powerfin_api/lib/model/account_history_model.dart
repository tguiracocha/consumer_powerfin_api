import 'package:flutter/material.dart';

class AccountHistory {
  String accountId;
  String date;
  String personId;
  String balance;
  String docNumber;

  AccountHistory({@required this.accountId,
    @required this.date,
    @required this.personId,
    @required this.balance});

  AccountHistory.fromMap(Map<String,dynamic> map) {
    accountId=map['accountId'];
    date= map['date'];
    personId= map['personId'];
    balance= map['balance'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['accountId'] = this.accountId;
    data['date'] = this.date;
    data['personId'] = this.personId;
    data['balance'] = this.balance;

    return data;
  }

}
