import 'payment_type_invoice.dart';
import 'branch.dart';
import 'item.dart';

class Cart {

  final Branch branch;
  List<Item> items;

  int totalItems=0;
  double totalToPay=0.00;
  double subtotalPaymentOnDelivery=0.00;
  double subtotalCredit=0.00;

  Cart({this.branch, this.items});

  Cart addProduct(Item product, PaymentTypeInvoice paymentTypeInvoice) {
    int index = items.indexWhere((item) => item.itemId == product.itemId);

    if (index == -1)
      items.add(product);
    else
      items[index].increaseQuantity();

    calculateTotalToPay(paymentTypeInvoice);
    calculateTotalProducts();

    return this;
  }

  Cart removeProduct(Item product, PaymentTypeInvoice paymentTypeInvoice) {
    int index = items.indexWhere((item) => item.itemId == product.itemId);
    items.removeAt(index);
    calculateTotalToPay(paymentTypeInvoice);
    calculateTotalProducts();
    return this;
  }

  Cart changeQuantity(
      Item product, int quantity, PaymentTypeInvoice paymentTypeInvoice) {
    if (quantity > 0 && quantity <= product.stock) {
      int index = items.indexWhere((item) => item.itemId == product.itemId);
      items[index].changeQuantity(quantity);
      calculateTotalToPay(paymentTypeInvoice);
      calculateTotalProducts();
    }

    return this;
  }

  ///===================================================================
  ///Calcular el total a pagar de acuerdo al tipo de pago de la factura
  ///y el tipo de pago del item (PAGO CONTRA ENTREGA Y CRÉDITO).
  ///Existen items que no se pueden vender a crédito El pago es
  ///contra-entrega.
  ///===================================================================

  Cart calculateTotalToPay(PaymentTypeInvoice paymentTypeInvoice) {

    const String CREDIT_PAYMENT_TYPE_ID='PPD';

    if (paymentTypeInvoice.paymentTypeId ==
        CREDIT_PAYMENT_TYPE_ID) {
      this.subtotalPaymentOnDelivery = items
          .map((Item item) =>
              (item.paymentTypeId != CREDIT_PAYMENT_TYPE_ID)
                  ? item.priceItem * item.quantity
                  : 0.00)
          .fold(0.0, (previous, next) => previous + next);

      this.subtotalCredit = items
          .map((item) =>
              (item.paymentTypeId == CREDIT_PAYMENT_TYPE_ID)
                  ? item.priceItem * item.quantity
                  : 0.00)
          .fold(0.0, (previous, next) => previous + next);
    } else {
      this.subtotalPaymentOnDelivery = items
          .map((item) => item.priceItem * item.quantity)
          .fold(0.0, (previous, next) => previous + next);
    }

    this.totalToPay = subtotalCredit + subtotalPaymentOnDelivery;

    return this;
  }

  Cart calculateTotalProducts() {

    if (this.items.length != 0) {
      this.totalItems = this.items
          .map((item) => item.quantity)
          .fold(0, (previous, next) => previous + next);
    }

    return this;
  }

}
