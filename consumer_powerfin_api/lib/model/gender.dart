class Gender {
  final String genderId;
  final String name;

  const Gender({this.genderId, this.name});

  factory Gender.fromJson(Map<String, dynamic> json) =>
      Gender(genderId: json['genderId'], name: json['name']);

  static getGenderList(var json) =>
      List<Gender>.from(json.map((i) => Gender.fromJson(i)));
  
}
