class PersonType {
  final String personTypeId;
  final String name;

  PersonType({this.personTypeId, this.name});

  static List<PersonType> getPersonTypes() => [
        PersonType(personTypeId: 'NAT', name: 'PERSONA NATURAL'),
        PersonType(personTypeId: 'LEG', name: 'PERSONA JURÍDICA')
      ];
}
