
import 'branch.dart';

class SaleCollected
{
  final String code;
  final String id;
  final double paymentValue;
  final String collectedDate;
  final String name;
  final Branch store;
  final String voucher;


  SaleCollected({
    this.code,
    this.id,
    this.paymentValue,
    this.collectedDate,
    this.name,
    this.store,
    this.voucher});

  factory SaleCollected.fromJson(var json){
    String _date = json.containsKey('collectedDate')?json['collectedDate']:'';
    int position = _date.indexOf('T');

    Branch _store = Branch.fromJson(json);

    SaleCollected saleCollected = new SaleCollected(
        code: json.containsKey('code')?json['code']:'',
        id: json.containsKey('id')?json['id']:'',
        paymentValue: json.containsKey('paymentValue')?json['paymentValue']:'',
        collectedDate: _date.substring(0,position),
        name: json.containsKey('name')?json['name']:'',
        voucher: json.containsKey('voucher')?json['voucher']:'',
        store: _store
    );

    return saleCollected;

  }

  static List<SaleCollected> getSaleCollectedList(var json)
  {

    List<SaleCollected> _saleCollectedList=[];

    if (json.length!=0)
    {

      for(var i=0; i<json.length;i++){
        SaleCollected _saleCollected = SaleCollected.fromJson(json[i]);
        if(_saleCollected.name!=null)
          _saleCollectedList.add(_saleCollected);
      }

    }

    return _saleCollectedList;
  }


  static double calculateTotalSale(List<SaleCollected> saleCollectedList) {

    double total=0.00;

    if(saleCollectedList.length!=0)
    {
      total = saleCollectedList
          .map((saleCollected)=> saleCollected.paymentValue)
          .fold(0, (previous, next) => previous + next);
    }
    return total;
  }


}