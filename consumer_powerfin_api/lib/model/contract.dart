
import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';

import 'account_payment_method.dart';

class Contract{

  final String contractId;
  final Person person;
  final Address instalationAddress;
  final InternetServicePlan servicePlan;
  final ZoneISP zoneISP;
  final AccountPaymentMethod accountPaymentMethod;
  final String remark;
  final String billingEmail;
  final bool applyFreeInstallation;
  final CustomerType customerType;
  final String document;

  Contract({
    this.contractId,
    this.person, 
    this.instalationAddress,
    this.servicePlan,
    this.zoneISP,
    this.accountPaymentMethod,
    this.remark,
    this.billingEmail,
    this.applyFreeInstallation,
    this.customerType,
    this.document
  });

  Map<String, dynamic> toJson()
  {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data["personId"]= this.person.personId;
    data["zoneId"]= this.zoneISP.id;
    data["applyFreeInstalation"]= this.applyFreeInstallation?1:0;
    data["customerTypeId"]= this.customerType.customerTypeId;
    data["installationAddressId"]= this.instalationAddress.personAddressId;
    data["serviceId"]= this.servicePlan.id;
    data["paymentMethod"]= this.accountPaymentMethod.toJsonISP();

    if(this.remark!=null)
      data["remark"]= "${this.remark}";

    if(this.billingEmail!=null)
      data["billingEmail"]= this.billingEmail!=''?this.billingEmail:'';

    print(data);
    return data;
  }
}