import 'dart:convert';

class AccountDetail {
  final double balanceCount;
  final double cartCount;
  final String categoryId;
  final String code;
  final String detailId;
  final String id;
  final String name;
  final double newPrice;
  final bool reviewFinalized;
  final int taxBase;
  final double countedQuantity;

  AccountDetail({
    this.balanceCount,
    this.cartCount,
    this.categoryId,
    this.code,
    this.detailId,
    this.id,
    this.name,
    this.newPrice,
    this.reviewFinalized,
    this.taxBase,
    this.countedQuantity
  });

  AccountDetail copyWith({
    double balanceCount,
    double cartCount,
    String categoryId,
    String code,
    String detailId,
    String id,
    String name,
    double newPrice,
    bool reviewFinalized,
    int taxBase,
    double countedQuantity
  }) =>
      AccountDetail(
        balanceCount: balanceCount ?? this.balanceCount,
        cartCount: cartCount ?? this.cartCount,
        categoryId: categoryId ?? this.categoryId,
        code: code ?? this.code,
        detailId: detailId ?? this.detailId,
        id: id ?? this.id,
        name: name ?? this.name,
        newPrice: newPrice ?? this.newPrice,
        reviewFinalized: reviewFinalized ?? this.reviewFinalized,
        taxBase: taxBase ?? this.taxBase,
        countedQuantity: countedQuantity?? this.countedQuantity
      );

  factory AccountDetail.fromRawJson(String str) => AccountDetail.fromJson(json.decode(str));

  static List<AccountDetail> getItemList(var json, {bool isDetail = false}) =>
      List<AccountDetail>.from(json.map((i) => AccountDetail.fromJson(i)));

  double getDiff() => balanceCount - countedQuantity;


  factory AccountDetail.fromJson(Map<String, dynamic> json) => AccountDetail(
    balanceCount: double.parse('${json['balanceCount']}'),
    cartCount: double.parse('${json["cartCount"]}'),
    categoryId: json["categoryId"],
    code: json["code"],
    detailId: json["detailId"],
    id: json["id"],
    name: json["name"],
    newPrice: json.containsKey('newPrice')?double.parse('${json["newPrice"]}'):0.00,
    reviewFinalized: json["reviewFinalized"],
    taxBase: 0,
    countedQuantity: json.containsKey("countedQuantity")?json["countedQuantity"]:0.00
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "detailId": detailId,
    "balanceCount":balanceCount,
    "cartCount":cartCount,
    "id": id,
    "categoryId": categoryId,
    "reviewFinalized": reviewFinalized,
    "countedQuantity":countedQuantity
  };



}
