import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';

class Person {
  final int personId;
  final String name;
  final PersonType personType;
  final String identification;
  final IdentificationType identificationTypeId;
  final String email;
  final String activity;
  final String priceListId;

  Person(
      { this.personId,
        this.name,
        this.personType,
        this.identification,
        this.identificationTypeId,
        this.email,
        this.activity,
        this.priceListId
      });

  factory Person.fromJson(var json, int personId) {
    return new Person(
      personId: personId,
      identification: json['identification'],
      name: json.containsKey('name') ? json['name'] : ''
    );
  }
}
