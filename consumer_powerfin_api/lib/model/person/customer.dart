import 'package:flutter/material.dart';

import '../../consumer_powerfin_api.dart';

class Customer extends Person {
  final String paternalSurname;
  final String maternalSurname;
  final String firstName;
  final String secondName;
  final Gender gender;
  final String birthDate;
  final MaritalStatus maritalStatus;
  final String cellphoneNumber;
  final String homePhoneNumber;
  final String cellphoneAreaCode;
  final String homephoneAreaCode;
  List<Address> address;
  final HomeType homeType;
  final String webPage;
  final double creditLimit;
  final bool allowPurchaseOrder;
  final String priceListId;
  final String reasonDenyPurchaseOrder;
  List<CustomerType> customerType;
  final bool disability;
  final bool elderly;

  Customer(
      {personId,
        @required name,
        @required identification,
        @required email,
        @required personType,
        @required identificationTypeId,
        @required activity,
        this.paternalSurname,
        this.maternalSurname,
        this.firstName,
        this.secondName,
        this.gender,
        this.birthDate,
        this.maritalStatus,
        this.cellphoneNumber,
        this.homePhoneNumber,
        this.cellphoneAreaCode,
        this.homephoneAreaCode,
        this.homeType,
        this.webPage,
        this.creditLimit,
        this.allowPurchaseOrder,
        this.priceListId,
        this.reasonDenyPurchaseOrder,
        this.disability,
        this.elderly,
        this.address,
        this.customerType})
      : super(
      identification: identification,
      personId: personId,
      name: name,
      email: email,
      personType:personType,
      identificationTypeId:identificationTypeId,
      activity:activity);

  factory Customer.fromJson(var json) {

    const String PRICE_LIST_ID = 'D';

    List<CustomerType> customerTypeList = [];
    List<Address> addresList = [];

    if (json.containsKey('addresses')) {
      if (json['addresses'].length > 0)
        addresList = Address.getAddressListFromJson(json['addresses']);
    }

    if (json.containsKey('customerTypes') && json['customerTypes'].length > 0)
      customerTypeList =
          CustomerType.getCustomerTypeList(json['customerTypes']);

    Customer _customer = new Customer(
      personType: json['personType'],
      personId: json['personId'],
      identification: json['identification'],
      identificationTypeId: IdentificationType.fromJson(json),
      activity: json['activity'],
      paternalSurname:
      json.containsKey('paternalSurname') ? json['paternalSurname'] : '',
      maternalSurname:
      json.containsKey('maternalSurname') ? json['maternalSurname'] : '',
      firstName: json.containsKey('firstName') ? json['firstName'] : '',
      secondName: json.containsKey('secondName') ? json['secondName'] : '',
      name: json.containsKey('name') ? json['name'] : '',
      email: json.containsKey('email') ? json['email'] : '',
      cellphoneNumber:
      json.containsKey('cellPhoneNumber') ? json['cellPhoneNumber'] : '',
      homePhoneNumber:
      json.containsKey('homePhoneNumber') ? json['homePhoneNumber'] : '',
      priceListId:
      json.containsKey('priceListId') ? json['priceListId'] : PRICE_LIST_ID,
      creditLimit: json.containsKey('creditLimit') ? json['creditLimit'] : 0.00,
      allowPurchaseOrder: json.containsKey('allowPurchaseOrder')
          ? json['allowPurchaseOrder']
          : false,
      reasonDenyPurchaseOrder: json.containsKey('reasonDenyPurchaseOrder')
          ? json['reasonDenyPurchaseOrder']
          : '',
      customerType: customerTypeList,
      address: addresList,
    );

    return _customer;
  }

  static List<Customer> getCustomerList(var json) =>
      List<Customer>.from(json.map((i) => Customer.fromJson(i)));

  Map<String, String> toJson() {
    if (this.personType.personTypeId == "LEG")
      return this._toJsonLegalPerson();
    else
      return this._toJsonNaturalPerson();
  }

  Map<String, String> _toJsonNaturalPerson() => {
    "personTypeId": "${this.personType.personTypeId}",
    "identification": "${this.identification}",
    "identificationTypeId":
    "${this.identificationTypeId.identificationTypeId}",
    "email": "${this.email}",
    "paternalSurname": "${this.paternalSurname}",
    "maternalSurname": "${this.maternalSurname}",
    "firstName": "${this.firstName}",
    "secondName": "${this.secondName}",
    "genderId": "${this.gender.genderId}",
    "birthDateAsString": "${this.birthDate}",
    "maritalStatusId": "${this.maritalStatus?.maritalStatusId}",
    "cellPhoneNumber": "${this.cellphoneNumber}",
    "cellPhoneAreaCode": "${this.cellphoneAreaCode}",
    "homePhoneNumber": "${this.homePhoneNumber}",
    "homePhoneAreaCode": "${this.homephoneAreaCode}",
    "activity": "${this.activity}",
    "homeMainStreet": "${this.address[0]?.homeMainStreet}",
    "homeSideStreet": "${this.address[0]?.homeSideStreet}",
    "homeNumber": "${this.address[0]?.homeNumber}",
    "districtId": "${this.address[0]?.district?.id}",
    "reference": "${this.address[0].reference}",
    "homeTypeId": "${this.homeType?.homeTypeId}",
    "latitude": "${this.address[0].position?.latitude}",
    "longitude": "${this.address[0].position?.longitude}",
    "disability": this.disability ? "1" : "0",
    "elderly": this.elderly ? "1" : "0",
  };

  Map<String, String> _toJsonLegalPerson() => {
    "personTypeId": "${this.personType.personTypeId}",
    "identification": "${this.identification}",
    "identificationTypeId":
    "${this.identificationTypeId.identificationTypeId}",
    "email": "${this.email}",
    "name": "${this.name}",
    "incorporationDateAsString": "${this.birthDate}",
    "cellPhoneNumber": "${this.cellphoneNumber}",
    "cellPhoneAreaCode": "${this.cellphoneAreaCode}",
    "homePhoneNumber": "${this.homePhoneNumber}",
    "homePhoneAreaCode": "${this.homephoneAreaCode}",
    "activity": "${this.activity}",
    "homeMainStreet": "${this.address[0]?.homeMainStreet}",
    "homeSideStreet": "${this.address[0]?.homeSideStreet}",
    "homeNumber": "${this.address[0]?.homeNumber}",
    "districtId": "${this.address[0]?.district?.id}",
    "reference": "${this.address[0].reference}",
    "homeTypeId": "${this.homeType?.homeTypeId}",
    "latitude": "${this.address[0].position?.latitude}",
    "longitude": "${this.address[0].position?.longitude}",
  };
}