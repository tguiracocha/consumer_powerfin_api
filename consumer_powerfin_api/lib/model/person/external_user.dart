import 'package:flutter/material.dart';

import '../../consumer_powerfin_api.dart';

class ExternalUser extends Person {
  final String paternalSurname;
  final String maternalSurname;
  final String firstName;
  final String secondName;
  final String homePhoneNumber;
  final String homephoneAreaCode;
  final String cellphoneNumber;
  final String cellphoneAreaCode;
  final Branch branch;
  final bool isActive;
  User user;
  String userId;

  ExternalUser(
      {@required personId,
      @required name,
      @required identification,
      @required email,
      @required this.userId,
      this.paternalSurname,
      this.maternalSurname,
      this.firstName,
      this.secondName,
      this.homePhoneNumber,
      this.homephoneAreaCode,
      this.cellphoneNumber,
      this.cellphoneAreaCode,
      this.branch,
      this.user,
      this.isActive})
      : super(
            personId: personId,
            name: name,
            identification: identification,
            email: email);

  factory ExternalUser.fromJson(var json) {
    ExternalUser _externalUser = new ExternalUser(
        personId: json['personId'],
        identification: json['identification'],
        paternalSurname:
            json.containsKey('paternalSurname') ? json['paternalSurname'] : '',
        maternalSurname:
            json.containsKey('maternalSurname') ? json['maternalSurname'] : '',
        firstName: json.containsKey('firstName') ? json['firstName'] : '',
        secondName: json.containsKey('secondName') ? json['secondName'] : '',
        name: json.containsKey('name') ? json['name'] : '',
        email: json.containsKey('email') ? json['email'] : '',
        homePhoneNumber: json['homePhoneNumber'],
        cellphoneNumber:
            json.containsKey('cellPhoneNumber') ? json['cellPhoneNumber'] : '',
        isActive: json['userActive'],
        branch: Branch.fromJson(json),
        userId: json.containsKey('userName') ? json['userName'] : '');

    return _externalUser;
  }
}
