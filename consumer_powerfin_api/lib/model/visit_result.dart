class VisitResult {
  final String visitResultId;
  final String visitResultName;
  final String account;
  final String numberDoc;
  final String balance;

  VisitResult(
      {this.visitResultId,
      this.visitResultName,
      this.account,
      this.numberDoc='',
      this.balance='0.00'});

  factory VisitResult.fromJson(var json) {
    return VisitResult(
        visitResultId: json.containsKey('visitResultTypeId')
            ? json['visitResultTypeId']
            : null,
        visitResultName: json.containsKey('name') ? json['name'] : null);
  }

  List<Map<String, dynamic>> toJson(List<VisitResult> visitResultList) {
    List<Map<String, dynamic>> visitResultListJson = [];

    for (int i = 0; i < visitResultList.length; i++) {
      final Map<String, dynamic> data = new Map<String, dynamic>();

      data['visitResultTypeId'] = visitResultList[i].visitResultId;

      if (visitResultList[i].account != null && visitResultList[i].account != '') {
        data['code'] = visitResultList[i].account.toUpperCase();
      }

      if(visitResultList[i].numberDoc!='') {
        data['documentNumber'] = "${visitResultList[i].numberDoc}";
      }

      if(visitResultList[i].balance !='')
        data['value'] = "${visitResultList[i].balance}";

      visitResultListJson.add(data);
    }

    print(visitResultListJson);
    return visitResultListJson;
  }

  static List<VisitResult> getVisitResult(var json) {
    List<VisitResult> visitPaymentList = [];

    if (json.length > 0) {
      for (int i = 0; i < json.length; i++) {
        VisitResult _visitResult = VisitResult.fromJson(json[i]);
        visitPaymentList.add(_visitResult);
      }
    }

    return visitPaymentList;
  }


}
