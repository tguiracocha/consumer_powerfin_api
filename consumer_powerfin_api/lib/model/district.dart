class District {
  final String id;
  final String name;
  final City city;
  final Country country;
  final StateAddress state;
  final Region region;

  District(
      {this.city, this.country, this.state, this.region, this.id, this.name});

  District.fromJson(Map json)
      : id = json['districtId'].toString(),
        name = json['name'],
        city = City.fromJson(json['city']),
        country = Country.fromJson(json['country']),
        region = Region.fromJson(json['region']),
        state = StateAddress.fromJson(json['state']) {
    print('In District.fromJson $name');
  }

  Map<String, String> toJson() => {"districtId": "${this.id}"};

  static List<District> getDistrictsfromJson(var json) =>
      List<District>.from(json.map((i) => District.fromJson(i)));
}

class City {
  final String id;
  final String name;

  City({this.id, this.name});

  factory City.fromJson(Map json) =>
      City(id: json['cityId'].toString(), name: json['name']);
}

class Country {
  final String id;
  final String name;

  Country({this.id, this.name});

  factory Country.fromJson(Map json) =>
      Country(id: json['countryId'].toString(), name: json['name']);
}

class Region {
  final String id;
  final String name;

  Region({this.id, this.name});

  factory Region.fromJson(Map json) =>
      Region(id: json['regionId'].toString(), name: json['name']);
}

class StateAddress {
  final String id;
  final String name;

  StateAddress({this.id, this.name});

  factory StateAddress.fromJson(Map json) =>
      StateAddress(id: json['stateId'].toString(), name: json['name']);
}
