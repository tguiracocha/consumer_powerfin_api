
class HostData {
  final String apiKey;
  final String baseUrl;
  final String token;

  HostData({this.apiKey, this.baseUrl, this.token});
}
