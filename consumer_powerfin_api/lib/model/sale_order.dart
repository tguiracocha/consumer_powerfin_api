import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';
import 'package:consumer_powerfin_api/model/sale_order_detail.dart';

import 'person/customer.dart';
import 'person/external_user.dart';



enum ParameterType { PersonId, ExternalUser, AccountStatus }

class SaleOrder {
  String id;
  final String dueDate;
  Customer customer;
  final ExternalUser externalSeller;
  SaleOrderDetail detail;
  final Address deliveryAddress;
  final String remark;
  final bool offRoute;
  final String authorizeInvoice;
  final PaymentTypeInvoice paymentTypeInvoice;
  final String channelId;
  PaymentMethod paymentMethod;

  SaleOrder(
      {this.id,
        this.dueDate,
        this.remark,
        this.offRoute,
        this.customer,
        this.externalSeller,
        this.detail,
        this.deliveryAddress,
        this.paymentTypeInvoice,
        this.authorizeInvoice = "0",
        this.channelId = DEFAULTCHANNELID,
        this.paymentMethod
      });

  //ESTADO DE LA CUENTA: SOLICITADO => 001 - ACTIVA=>002
  static const String REQUEST_STATUS = '001';

  static const String DEFAULTCHANNELID='003';

  static List<SaleOrder> getSaleOrders(var json, ExternalUser seller) =>
      List<SaleOrder>.from(
          json.map((_saleOrder) => SaleOrder.fromJson(_saleOrder, seller)));

  factory SaleOrder.fromJson(var json, ExternalUser externalUser) => SaleOrder(
      id: json.containsKey('id') ? json['id'] : '',
      dueDate: json.containsKey('dueDate') ? json['dueDate'] : '',
      offRoute: json.containsKey('offRoute') ? json['offRoute'] : '',
      customer: Customer.fromJson(json['person']),
      externalSeller: externalUser,
      paymentTypeInvoice: json.containsKey('paymentType')
          ? PaymentTypeInvoice.fromJson(json['paymentType'])
          : null);

  Map<String, dynamic> toJsonSaleOrder() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    List<Map> items = [];

    if (detail.items.length > 0)
      detail.items.forEach((item) => items.add(item.toJson()));

    data["posId"] = "${externalSeller.branch.posId}";
    data["personId"] = "${this.customer.personId}";
    data["totalAmount"] = "${this.detail.totalToPay}";
    data["remark"] = "${this.remark}";
    data["offRoute"] = "${this.offRoute}";
    data["authorizeInvoice"] = "${this.authorizeInvoice}";


    data["paymentTypeId"] = this.paymentTypeInvoice.paymentTypeId;

    if (this.paymentMethod != null)
      data["paymentMethodList"] = [this.paymentMethod.toJson()];

    data["accountItemList"] = items;

    if (this.channelId != null) data["channelId"] = this.channelId;

    if(this.deliveryAddress!=null)
      data["personAddressId"]=this.deliveryAddress.personAddressId;


    if (this.deliveryAddress != null)
      data["personAddressId"] = this.deliveryAddress.personAddressId;

    if (this.dueDate != null) data["dueDate"] = this.dueDate;

    print(data);

    return data;
  }

  static Map<String, String> toJsonByParameterType(
      ParameterType parameterType, String parameter) {
    switch (parameterType) {
      case ParameterType.PersonId:
        return {"id": "$parameter", "accountStatusId": REQUEST_STATUS};
      case ParameterType.AccountStatus:
        return {"accountStatusid": REQUEST_STATUS};
      case ParameterType.ExternalUser:
        return {"user": "$parameter", "accountStatusId": REQUEST_STATUS};
    }

    return {};
  }

  /// Validar el Crédito: Revisamos si el item ya se encuentra en el detalle
  /// de la orden de venta. Si no esta en el detalle multiplica la cantidad
  /// por el precio del item mas el subtotal de credito de toda la orden de venta.
  /// Si quantity es igual a cero NO ES UN CAMBIO DE CANTIDAD EN EL ITEM
  /// es un incremento en UNO multiplicamos la cantidad por el precion mas el
  /// subtotal de crédito de toda la orden de venta. Si quantity NO ES CERO
  /// es un cambio de cantidad (ingreso por teclado) el valor, multiplica cantidad
  /// por precio del item mas el subtotal del crédito de toda la orden de venta.
  /// sin el total del item ingresado anteriormente.

  static bool validateCredit(Item item, SaleOrder saleOrder,
      {int quantity = 0}) {
    SaleOrder _saleOrder = saleOrder;

    const String CREDIT_PAYMENT_TYPE_ID = 'PPD';
    bool hasCredit = true;
    double _totalItem = 0.00;

    /// Si el método de pago de la orden de venta como del item a CRÉDITO

    if (_saleOrder.paymentTypeInvoice.paymentTypeId == CREDIT_PAYMENT_TYPE_ID &&
        item.paymentTypeId == CREDIT_PAYMENT_TYPE_ID) {
      int index = _saleOrder.detail.items
          .indexWhere((_item) => _item.itemId == item.itemId);

      if (index == -1)
        _totalItem = item.priceItem * item.quantity;
      else {
        if (quantity == 0) {
          _totalItem = (item.priceItem * 1) + _saleOrder.detail.subtotalCredit;
        } else {
          double a = item.priceItem * item.quantity;
          double b = item.priceItem * quantity;
          print('$a');
          print('$b');
          _totalItem = (_saleOrder.detail.subtotalCredit + b) - a;
        }
      }
      if (_totalItem > _saleOrder.customer.creditLimit) hasCredit = false;
    }

    return hasCredit;
  }
}
