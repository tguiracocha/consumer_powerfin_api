import 'package:consumer_powerfin_api/consumer_powerfin_api.dart';

class CustomerType{

  final String customerTypeId;
  final String name;

  CustomerType({this.customerTypeId, this.name});

  CustomerType.fromJson(Map<String, dynamic> json)
      :customerTypeId= json['customerTypeId'],
        name= json['name']{
    print('In CustomerType.fromJson: $name');
  }

  static getCustomerTypeList(var json)=>List<CustomerType>
      .from(json.map((i) => CustomerType.fromJson(i)));

  Map<String, dynamic> toJsonCustomerType(Customer person) {

    final Map<String, dynamic> data = new Map<String, dynamic>();
    if(person.identificationTypeId!=null)
      data['identificationTypeId'] ='${person.identificationTypeId.identificationTypeId}';
    data['email'] = person.email;
    data['paternalSurname'] = '${person.paternalSurname}';
    data['firstName'] ='${person.firstName}';
    data['identification'] = '${person.identification}';
    data['personId'] = "${person.personId}";
    data['customerTypes'] = [
      {
        "customerTypeId": "${this.customerTypeId}",
        "name": "${this.name}"
      }
    ];

    print('CustomerType: '+ data.toString());
    return data;

  }

}