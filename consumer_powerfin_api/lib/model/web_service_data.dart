import '../consumer_powerfin_api.dart';
import 'application.dart';

class WebServiceData {
  final String apiKey;
  final String baseURL;
  final String company;
  User userActive;

  WebServiceData(this.baseURL, this.apiKey, {this.company, this.userActive});

  factory WebServiceData.fromJson({var jsonMap, String organization}) {
    WebServiceData webServiceData;

    if (jsonMap.length != 0) {
      for (var i = 0; i < jsonMap.length; i++) {
        if (jsonMap[i]['organization'] == organization) {
          webServiceData = WebServiceData(
              jsonMap[i]['data']["baseURL"], jsonMap[i]['data']["api_key"],
              company: jsonMap[i]['data']["organization"]);
        }
      }
    }

    return webServiceData;
  }

  WebServiceData copyWith(User user) {
    WebServiceData webServiceData;

    if (user == null) {
      webServiceData = WebServiceData(baseURL, apiKey, company: company);
    } else {
      webServiceData =
          WebServiceData(baseURL, apiKey, company: company, userActive: user);
    }

    return webServiceData;
  }

  factory WebServiceData.fromApplication(Application application, {User user}) {
    WebServiceData webServiceData;

    if (user == null) {
      webServiceData = WebServiceData(application.baseUrl, application.apiKey,
          company: application.company);
    } else {
      webServiceData = WebServiceData(application.baseUrl, application.apiKey,
          company: application.company, userActive: user);
    }

    return webServiceData;
  }
}
