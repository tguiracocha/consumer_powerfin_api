
class PhoneNumberType{

  final String phoneNumberTypeId;
  final String name;

  PhoneNumberType({
    this.phoneNumberTypeId,
    this.name
  });

  factory PhoneNumberType.fromJson(Map json)=>
      PhoneNumberType(
          phoneNumberTypeId: json['phoneNumberTypeId'],
          name: json['name']);


  static getPhoneNumberTypefromJson(var json)=>List<PhoneNumberType>
      .from(json.map((i) => PhoneNumberType.fromJson(i)));


}