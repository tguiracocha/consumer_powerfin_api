
class MaritalStatus{
  String maritalStatusId;
  String name;

  MaritalStatus({
    this.maritalStatusId,
    this.name});

  factory MaritalStatus.fromJson(Map<String, dynamic> json)=>MaritalStatus(
        maritalStatusId: json['maritalStatusId'],
        name: json['name']
    );

  static getMaritalStatusList(var json)=>List<MaritalStatus>
      .from(json.map((i) => MaritalStatus.fromJson(i)));



}