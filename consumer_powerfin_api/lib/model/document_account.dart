
import 'package:flutter/material.dart';

///Documentos de una cuenta: Para cargar documentos del
///contrato de internet cédula/factura de pago de luz

class DocumentAccount {

  final String account;
  final String data;
  String _name = 'documento';
  String _type = 'jpg';

  String get name => _name;

  String get type => _type;

  DocumentAccount({@required this.account, @required this.data});

  Map<String, dynamic> toJson() {

    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["name"] = this.name.toLowerCase();
    data["dataType"] = this.type;
    data["entityId"] = this.account;
    data["data"] = this.data;
    return data;

  }

}
