
class ZoneISP {

  final int id;
  final String name;
  final int districtId;
  final String districtName;

  ZoneISP({this.id, this.name, this.districtId, this.districtName});

  factory ZoneISP.fromJson(Map json)=>
      ZoneISP(
          id:json['zoneId'],
          name:json['name'],
          districtId: json['districtId'],
          districtName: json['districtName']);


  static getZoneISPfromJson(var json)=>List<ZoneISP>
      .from(json.map((i) => ZoneISP.fromJson(i)));


}