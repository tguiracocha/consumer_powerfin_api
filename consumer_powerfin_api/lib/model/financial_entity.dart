
class FinancialEntity{

  final String id;
  final String name;

  FinancialEntity({
    this.id,
    this.name
  });

  factory FinancialEntity.fromJson(Map<String, dynamic> json)=>
      FinancialEntity(
        id: json['financialEntityId'],
        name: json['name']);


  static getFinantialEntityList(var json)=>List<FinancialEntity>
        .from(json.map((i) => FinancialEntity.fromJson(i)));

}