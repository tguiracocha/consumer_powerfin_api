class HomeType {
  final String homeTypeId;
  final String name;

  HomeType({this.homeTypeId, this.name});

  HomeType.fromJson(Map json)
      :homeTypeId= json['homeTypeId'],
        name= json['name']{
    print('In HomeType.fromJson:' + name);
  }

  static getHomeTypesfromJson(var json) =>
      List<HomeType>.from(json.map((homeType) => HomeType.fromJson(homeType)));

  @override
  String toString() {
    return '$name';
  }

}
