//MÉTODOS DE PAGO VENTAS EXTERNAS CHEQUE, TRANSFERENCIA, EFECTIVO

import 'package:flutter/material.dart';

class PaymentMethod {
  final String paymentMethodId;
  final String paymentMethodName;
  final IconData iconData;
  bool selected;
  double total;

  PaymentMethod(
      {this.paymentMethodId,
      this.paymentMethodName,
      this.total = 0.00,
      this.iconData = Icons.add,
      this.selected = false});

  factory PaymentMethod.fromJson(var json) => PaymentMethod(
        paymentMethodId: json.containsKey('paymentMethodId')
            ? json['paymentMethodId']
            : null,
        paymentMethodName: json.containsKey('name') ? json['name'] : null,
      );


  static List<PaymentMethod> getPaymentMethodList(var json) =>
      List<PaymentMethod>.from(json.map((i) => PaymentMethod.fromJson(i)));


  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['paymentMethodId'] = '${this.paymentMethodId}';
    data["name"] = this.paymentMethodName;
    return data;
  }

}
