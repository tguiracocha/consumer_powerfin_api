

import 'package:flutter/material.dart';

class PaymentTypeInvoice {
  final String paymentTypeId;
  final String paymentTypeName;
  final IconData iconData;
  final String message;
  final double minimumAmount;
  final bool canOrder;

  PaymentTypeInvoice(this.paymentTypeId, this.paymentTypeName,
      {this.iconData, this.canOrder = false, this.message = '', this.minimumAmount=0.0});

  factory PaymentTypeInvoice.fromJson(var json) {
    return PaymentTypeInvoice(
      json.containsKey('paymentTypeId') ? json['paymentTypeId'] : null,
      json.containsKey('name') ? json['name'] : null,
      minimumAmount:json.containsKey('minimumAmount') ? json['minimumAmount'] : 0.00,
      iconData: Icons.attach_money
    );
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();

    data['paymentTypeId'] = this.paymentTypeId;
    return data;
  }

  PaymentTypeInvoice copyWith(String message, bool canOrder) {

    return PaymentTypeInvoice(
        this.paymentTypeId,
        this.paymentTypeName,
        iconData: this.iconData,
        minimumAmount: this.minimumAmount,
        message: message == null? this.message:message,
        canOrder: canOrder==false?this.canOrder:canOrder
    );
  }

  static List<PaymentTypeInvoice> getPaymentTypeList(var json) =>List<PaymentTypeInvoice>
      .from(json.map((i) => PaymentTypeInvoice.fromJson(i)));


}
