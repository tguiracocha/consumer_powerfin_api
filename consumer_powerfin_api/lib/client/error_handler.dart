import 'dart:io';

import 'package:dio/dio.dart';

class ErrorHandler {

  static String handleError(dynamic e, String filter) {

    String _error = 'Error desconocido - $filter';

    if (e is DioError) {

      if (e.type == DioErrorType.RESPONSE) {

        final response = e.response;

        try {
          if (response != null) {

            final Map responseData = response.data;
            _error = "Status  ${responseData['status']} - " +
                responseData['message'];
          }

        } catch (e) {

          _error = "Internal Error Catch";

        }
      } else if (e.type == DioErrorType.CONNECT_TIMEOUT ||
          e.type == DioErrorType.RECEIVE_TIMEOUT ||
          e.type == DioErrorType.SEND_TIMEOUT) {

        _error = "Status 408 Request timeout";

      } else if (e.type == DioErrorType.DEFAULT) {

        _error = '${e.error} {$filter}';

      } else if (e.error is SocketException) {

        _error = "No Internet Connection!";

      } else {

        _error = '$e - $filter';

      }
    } else {

      _error = e.toString();

    }

    return _error;
  }
}

