import 'dart:io';

import 'package:consumer_powerfin_api/client/error_handler.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import '../utils/verify_conectivity.dart';

class ApiProvider {
  static Future<dynamic> getData({
    @required String filter,
    @required Dio dio,
    Map<String, String> parameters,
  }) async {
    dio.options.queryParameters = parameters;

    final state = await initConnectivity();

    try {
      if (state) {
        Response response = await dio.get(filter);

        if (response.statusCode == 200) return response.data['objectList'];
      } else {
        throw ('Sin conexión');
      }
    } catch (error) {
      throw ErrorHandler.handleError(error, filter);
    }
  }

  static Future<dynamic> postData({
    @required Dio dio,
    @required String filter,
    @required Map data,
    bool isLogin = false,
  }) async {
    print(data);

    final state = await initConnectivity();

    try {
      if (state) {
        Response response = await dio.post(
          filter,
          data: data,
        );

        if (response.statusCode == 200) {
          if (isLogin) return response.headers['Authorization'][0];

          return response.data['objectList'];
        }
      } else {
        throw Exception('Sin conexión');
      }
    } catch (error) {
      throw ErrorHandler.handleError(error, filter);
    }
  }

  static Future<dynamic> putData(
      {String filter, @required Dio dio, Map data}) async {
    final state = await initConnectivity();

    try {
      if (state) {
        Response response = await dio.put(
          filter,
          data: data,
        );

        return response.data;
      } else {
        throw 'Sin conexión';
      }
    } catch (error) {
      throw ErrorHandler.handleError(error, filter);
    }
  }

  static Future<dynamic> deleteData(
      {String filter, @required Dio dio, Map data}) async {
    final state = await initConnectivity();

    try {
      if (state) {
        Response response = await dio.delete(
          filter,
          data: data,
        );

        return response.data;
      } else {
        throw ErrorHandler.handleError(
            new SocketException('Without Conexion'), filter);
      }
    } catch (error) {
      throw ErrorHandler.handleError(error, filter);
    }
  }
}
